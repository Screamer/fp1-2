//  FP Penning trap trigger box
//  v2019.11

//Pins according to original wiring of the trigger box:
int RIGOLpin = 2;
int DETECTpin = 5;
int EXCITATIONpin = 6;
int LOADpin = 7; 

//timings have to be given in ms:

float LOADtime = 500;
float WAIT1 = 5;
float EXCITATIONtime = 10;
float WAIT2 = 50;
float DETECTtime = 50;
float WAIT3 = 500;
float RIGOLtime = 50;
/*
float LOADtime = 200;
float WAIT1 = 1;
float EXCITATIONtime = 2;
float WAIT2 = 50;
float DETECTtime = 50;
float WAIT3 = 0;
float RIGOLtime = 50;
*/

bool continuous = false;

//variables used for splitting the input string
int lastIndex;
int nextIndex;
bool getValues = false;

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.setTimeout(100);
  Serial.write("ready\n");
  pinMode(DETECTpin, OUTPUT); // DETECT TRIGGER
  pinMode(EXCITATIONpin, OUTPUT); // EXCITATION TRIGGER
  pinMode(LOADpin, OUTPUT); // LOAD TRIGGER
  pinMode(RIGOLpin, OUTPUT); // RIGOL TRIGGER
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LOADpin, HIGH);
  digitalWrite(EXCITATIONpin, LOW);
  digitalWrite(LOADpin, LOW);
  digitalWrite(RIGOLpin, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  }

void loop(){

  String input = Serial.readStringUntil('\n');
  
  if (input != "") {
    if(input.substring(0,4) == "trig") {
      digitalWrite(LOADpin, LOW);
      delay(LOADtime);
      digitalWrite(LOADpin, HIGH);
      delay(WAIT1);
      
      digitalWrite(EXCITATIONpin, HIGH);
      delay(EXCITATIONtime);
      digitalWrite(EXCITATIONpin, LOW);
      delay(WAIT2);
      
      digitalWrite(RIGOLpin, HIGH);
      delay(RIGOLtime);
      digitalWrite(RIGOLpin, LOW);
      delay(WAIT3);
      
      digitalWrite(DETECTpin, HIGH);
      delay(DETECTtime);
      digitalWrite(DETECTpin, LOW);
      
    } else if(input.substring(0,4) == "load"){
      LOADtime = input.substring(5).toFloat();
    } else if(input.substring(0,5) == "wait1"){
      WAIT1 = input.substring(6).toFloat();
    } else if(input.substring(0,6) == "excite"){
      EXCITATIONtime = input.substring(7).toFloat();
    } else if(input.substring(0,5) == "wait2"){
      WAIT2 = input.substring(6).toFloat();
    } else if(input.substring(0,6) == "detect"){
      DETECTtime = input.substring(7).toFloat();
    } else if(input.substring(0,5) == "wait3"){
      WAIT3 = input.substring(6).toFloat();
    } else if(input.substring(0,5) == "rigol"){
      RIGOLtime = input.substring(6).toFloat();
    } else if(input.substring(0,5) == "times"){
      
      lastIndex = input.indexOf(" ", 0);
      nextIndex = input.indexOf(" ", lastIndex + 1);
      LOADtime = input.substring(lastIndex, nextIndex).toFloat();
      lastIndex = nextIndex;
      
      nextIndex = input.indexOf(" ", lastIndex + 1);
      WAIT1 = input.substring(lastIndex, nextIndex).toFloat();
      lastIndex = nextIndex;
      
      nextIndex = input.indexOf(" ", lastIndex + 1);
      EXCITATIONtime = input.substring(lastIndex, nextIndex).toFloat();
      lastIndex = nextIndex;
  
      nextIndex = input.indexOf(" ", lastIndex + 1);
      WAIT2 = input.substring(lastIndex, nextIndex).toFloat();
      lastIndex = nextIndex;
      
      nextIndex = input.indexOf(" ", lastIndex + 1);
      DETECTtime = input.substring(lastIndex, nextIndex).toFloat();
      lastIndex = nextIndex;
  
      nextIndex = input.indexOf(" ", lastIndex + 1);
      WAIT3 = input.substring(lastIndex, nextIndex).toFloat();
      lastIndex = nextIndex;
      
      nextIndex = input.indexOf(" ", lastIndex + 1);
      RIGOLtime = input.substring(lastIndex, nextIndex).toFloat(); 
  
    } else if(input.substring(0,10) == "start_cont"){
      continuous = true;
    } else if(input.substring(0,9) == "stop_cont"){
      continuous = false;
    }
  }

  if (continuous == true) {
    digitalWrite(LOADpin, LOW);
    delay(LOADtime);
    digitalWrite(LOADpin, HIGH);
    delay(WAIT1);
    
    digitalWrite(EXCITATIONpin, HIGH);
    delay(EXCITATIONtime);
    digitalWrite(EXCITATIONpin, LOW);
    delay(WAIT2);
    
    digitalWrite(RIGOLpin, HIGH);
    delay(RIGOLtime);
    digitalWrite(RIGOLpin, LOW);
    delay(WAIT3);
    
    digitalWrite(DETECTpin, HIGH);
    delay(DETECTtime);
    digitalWrite(DETECTpin, LOW);
  }
}
