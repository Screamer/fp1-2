import devices
import time
import os
import numpy as np

#============================================
# Definition of constants / measurement range
#============================================

#create path for datafiles (ADD YOUR GROUP NAME!):
today = time.strftime("%Y_%m_%d_%H_%M_%S")
directoryname = today + "_FullRangeScan"
if not os.path.exists(directoryname):
    os.makedirs(directoryname)
path = directoryname

#Trigger timings
load = 500
wait1 = 5
excite = 10
wait2 = 5
rigol = 5   # the Rigol spectrum analyzer has a 30 ms trigger delay
wait3 = 0
detect = 50
cycle_time = load + wait1 + excite + wait2 + rigol + wait3 + detect
     
devices.set_time('load', load)
devices.set_time('wait1', wait1)
devices.set_time('excite', excite)
devices.set_time('wait2', wait2)
devices.set_time('detect', detect)
devices.set_time('wait3', wait3)
devices.set_time('rigol', rigol)

#number of averages per cycle
average_num = 5 # TODO

#trap ring voltage0,0
ring_voltages = [44.6,45.6,46.6] # TODO: Enter your set of ring voltages

#magnet coil currents
coil_currents = [1.0,1.1,1.2,1.3] # TODO: Enter your set of coil currents

#excitation power in dBm
epower = [8,15] # TODO: Enter your set of excitation powers


#frequency scan range and step size

freq_min = 1
freq_max = 500
freq_step = 0.1

#finalize setup for measurement: set spectrum analyzer to zero span mode
center_freq="56.615MHz"
ref_level = "-19"
PDIV_scale = "2.5"
sweep_time="60ms"
devices.set_analyzer_to_zero_span(center_freq, ref_level, 
                              PDIV_scale, sweep_time)

print("Start...")

#============================================
# MEASUREMENT FUNCTION
#============================================

def scanFreq(freq_min, freq_max, freq_step, rvolt, ccurrent, excitation_power):
    """
    This function scans over the frequency of the antenna excitation signal starting from freq_min until freq_max in steps of freq_step.
    For each step it reads the trace of the SDA815 spectrum analyzer and calculates the MMD (maximum-minimum-depth), averages it (depending
    on your settings) and writes it into a file.
    """

    #epoch_time = int(time.time()) # this function saves the current time (as kept by the computer) into epoch_time

    """
    Specifies the filename starting with the date and the names of your individual measurements.
    When you use this function later choose something meaningful for "name" in each individual measurement
    e.g. add the ring voltage, coil current, excitation power, frequency range, ...
    In this way it is easier to analyze the data later.
    """

    #define the filename and create/open datafile
    fname = time.strftime("%Y_%m_%d_%H_%M_%S") + "_" + str(rvolt) + "V_" + str(ccurrent) + "A_" + str(freq_min) + "-" + str(freq_max) + "MHz_" + str(excitation_power) + "dBm.csv"
    file = open(os.path.join(path, fname), "w+")
    file_flush_counter = 0  #counts the amount of data points saved into the buffer (the computer's temporary storage space)
    devices.set_ring_voltage(rvolt) #sets the ring voltage to given value
    devices.set_coil_current(ccurrent) #sets the coil current to given value
    devices.excitation_on() #switch on the output of the excitation
    devices.set_excitation_power(excitation_power) #sets the excitation power to given value
    for f in np.arange(freq_min, freq_max+freq_step, freq_step): #scan the frequencies from freq_min in steps of size freq_step until freq_max
        devices.set_excitation_frequency(f) #set the current excitation frequency to f
        mmr_av = []
        for ii in range(average_num):
            try:
                devices.send_trigger()
            except:
                print("trigger failed")
                try:
                    devices.trigger.clear()
                    time.sleep(0.3)
                    devices.send_trigger()
                except:
                    print("trigger failed double")
                    devices.trigger.clear()
                    devices.fix_arduino_comm()
                    time.sleep(10)
                    devices.send_trigger()
            try:
                devices.wait_for_trigger_finish()
            except:
                time.sleep((cycle_time+150)/1000) #sleep in seconds, a bit longer than the measurement cycle
            time.sleep(0.4)
            try:
                data = devices.get_analyzer_data_fast() #this function obtains the data currently shown in the DSA 815 - an array containing the vertical coordinates (MMD) of the points in the graph.
            except Exception as e:
                print("Error at get data")
                print(e)
                devices.analyzer.close()
                time.sleep(1)
                devices.analyzer = devices.open_analyzer_communication()
                time.sleep(1)
                devices.set_analyzer_to_zero_span(center_freq, ref_level, 
                              PDIV_scale, sweep_time)
                continue
            
            mmr_av.append(np.average(data[0:10]) - min(data[1:])) # calculates the MMD of the data. MMD = depth of deepest dip in graph
            mmr = np.average(mmr_av)
        
        print(str(f) + "," + str(mmr))  # prints the data point we just obtained - excitation signal frequency and the corresponding MMD to the shell.
        file.write(str(f) + "," + str(mmr) + "\n") # saves the data point into the buffer.
        # to save the data point into the file and not only into the buffer, we need to flush the file.
        # We choose to do this every 40 data points (an arbitrary number) and not every one data point
        # in order to make the program run faster.
        if(file_flush_counter >= 10): # we flush the file every 40 data points
            file.flush()  # not only write into buffer also to the file
            file_flush_counter = 0 # reset the buffer
        else: # if we didn't reach 40 yet,
            file_flush_counter += 1 # increase the count by 1
    file.close() # close the file


#============================================
# MEASUREMENT
#============================================

"""
PLACE YOUR ACTUAL MEASUREMENT CODE HERE AND USE THE ABOVE DEFINED FUNCTION AND THE FOLLOWING COMMANDS:

devices.set_coil_current(1.3)
devices.set_ring_voltage(rvolt)
devices.set_excitation_power(-12)
scanFreq(freq_min,freq_max,freq_step, ring_voltage, ccurent)
"""


for amplitude in epower:
    for spannung in ring_voltages:
        print("Spannung:", spannung, "V")
        print("Stromstärke: 1.1 A")
        print("Amplitude:", amplitude, 'dBM')
        scanFreq(freq_min, freq_max, freq_step, spannung, 1.1, amplitude)

for amplitude in epower:
    for strom in coil_currents:
        print("Spannung: 45.6 V")
        print("Stromstärke:", strom, "A")
        print("Amplitude:", amplitude, 'dBM')
        scanFreq(freq_min, freq_max, freq_step, 45.6, strom, amplitude)

del devices.hf_gen
del devices.analyzer

#Reset coil current to let the coil cool down
devices.set_coil_current(0.05)
