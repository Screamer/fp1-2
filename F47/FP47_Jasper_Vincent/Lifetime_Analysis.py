import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.stats as stats

def chisquareValue(xvalue, yvalues, sigma_y, fit_parameter, function):
    chi2=0
    for i in range(len(xvalue)):
        expected = function(xvalue[i],*fit_parameter)
        chi2+=((expected-yvalues[i])/sigma_y[i])**2
    prob = round(1 - stats.chi2.cdf(chi2, len(xvalue) - len(fit_parameter)), 4) * 100
    return chi2/(len(xvalue)-len(fit_parameter)), prob #chis2 per degree of freedom and probability

plt.style.use('classic')

wait, avg, std = np.genfromtxt('lifetime2.txt', delimiter=',', unpack=True)
start = 15  


fit_func = lambda t, N0, tau, off: N0*np.exp(-t/tau)+ off
guess = (40, 250, 0)
popt, pcov = curve_fit(fit_func, wait[start:], avg[start:], sigma=std[start:], p0=guess)
chi_ndf, prob = chisquareValue(wait[start:], avg[start:], std[start:], popt, fit_func)
print(popt)
print(pcov)

t = np.linspace(min(wait), max(wait), 100)
plt.errorbar(wait, avg, yerr=std, label='data')
plt.plot(t, fit_func(t, *guess), label='guess')
plt.plot(t, fit_func(t, *popt), label='fit')
plt.grid(True)
plt.text(400, 30, r'$\chi$ / ndf = {0}, P = {1} %'.format(round(chi_ndf, 2), round(prob, 2)), style='italic', bbox={'facecolor': 'gray', 'alpha': 0.2, 'pad': 5})
plt.text(400, 35, r'$\tau$ = {0} [s], $\Delta\tau$ = {1} [s]'.format(round(popt[1], 2), round(np.sqrt(pcov[1,1]), 2)), style='italic', bbox={'facecolor': 'gray', 'alpha': 0.2, 'pad': 5})

plt.title('Bestimmung der Halbwertszeit', size=20)
plt.ylabel('Elektronenanzahl', size=15)
plt.xlabel('Zeit [s]', size=15)
plt.legend()
plt.savefig('../results/Lebendsdauer.pdf', format='PDF')
plt.show()

