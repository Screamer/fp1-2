import scipy.constants as const
import numpy as np
import uncertainties as unc
import uncertainties.unumpy as unp
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as stats
from uncertainties.unumpy import sqrt

def read_data(dir, skiprows):
    data = pd.read_csv(dir, skiprows=skiprows, delimiter=',')
    data.columns = ['f', 'A']
    return data

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

def axial_frequ_fit(x, a):
    return a*np.sqrt(x)

def calc_values(V, I,):
    N = 2400
    L = 246 * const.milli
    C = 1.4e4
    B = const.mu_0 * N / L * I
    f_c = 1/(2*const.pi) * const.elementary_charge / const.m_e * B
    f_z = 1/(2*const.pi) * np.sqrt(const.elementary_charge * V * 2 * C / const.m_e)
    f_m = 1/2 * (f_c - np.sqrt(f_c**2 - 2 * f_z**2))
    f_p = 1/2 * (f_c + np.sqrt(f_c**2 - 2 * f_z**2))
    return f_c * const.micro, f_z * const.micro, f_m * const.micro, f_p * const.micro

def linear_fit(x,a):
    return a*x

def chisquareValue(xvalue, yvalues, sigma_y, fit_parameter, function):
    chi2=0
    for i in range(len(xvalue)):
        expected = function(xvalue[i],*fit_parameter)
        chi2+=((expected-yvalues[i])/sigma_y[i])**2
    prob = round(1 - stats.chi2.cdf(chi2, len(xvalue) - len(fit_parameter)), 4) * 100
    return chi2/(len(xvalue)-len(fit_parameter)), prob #chis2 per degree of freedom and probability

def plot_data(data, I, V, frequ_exp = None):
    plt.clf()
    plt.style.use('classic')
    fig, axs = plt.subplots(nrows = len(data), ncols=1, sharex = True, figsize=(50,50))
    fig.subplots_adjust(hspace=.3, wspace=0.2)
    fig.suptitle('Bestimmung der Zyklotronfrequenz - Experimentelle Werte', size=60)
    for i in range(len(data)):
        if frequ_exp is None:
            f_c, f_z, f_m, f_p = calc_values(V[i], I[i])
            d_f_c, d_f_z, d_f_m, d_f_p = [0, 0, 0, 0]
        else:
            f_c = unp.nominal_values(frequ_exp[0])
            f_z = unp.nominal_values(frequ_exp[1])
            f_m = unp.nominal_values(frequ_exp[2])
            f_p = unp.nominal_values(frequ_exp[3])

            d_f_c = unp.std_devs(frequ_exp[0])
            d_f_z = unp.std_devs(frequ_exp[1])
            d_f_m = unp.std_devs(frequ_exp[2])
            d_f_p = unp.std_devs(frequ_exp[3])

        axs[i].set_title(r'I = {0} A, U = {1} V'.format(I[i], V[i]), size=30)
        axs[i].plot(data[i]['f'], data[i]['A'] / np.amax(data[i]['A']), marker=None, ls='solid', color='darkred')

        if frequ_exp is not None:
            axs[i].axvspan(f_c[i] - d_f_c[i], f_c[i] + d_f_c[i], facecolor='gray', alpha=0.3)
            axs[i].axvspan(f_z[i] - d_f_z[i], f_z[i] + d_f_z[i], facecolor='gray', alpha=0.3)
            axs[i].axvspan(f_m[i] - d_f_m[i], f_m[i] + d_f_m[i], facecolor='gray', alpha=0.3)
            axs[i].axvspan(f_p[i] - d_f_p[i], f_p[i] + d_f_p[i], facecolor='gray', alpha=0.3)

        axs[i].vlines(f_c[i], 0, 1, color='darkblue')
        axs[i].vlines(f_z[i], 0, 1, color='darkblue')
        axs[i].vlines(f_m[i], 0, 1, color='darkblue')
        axs[i].vlines(f_p[i], 0, 1, color='darkblue')

        axs[i].text(f_c[i] + 1, 0.1, r'$f_c$ = {} MHz'.format(round(f_c[i], 0)), color='blue')
        axs[i].text(f_z[i] + 1, 0.1, r'$f_z$ = {} MHz'.format(round(f_z[i], 0)), color='blue')
        axs[i].text(f_m[i] + 1, 0.1, r'$f_-$ = {} MHz'.format(round(f_m[i], 0)), color='blue')
        axs[i].text(f_p[i] - 20, 0.1, r'$f_+$ = {} MHz'.format(round(f_p[i], 0)), color='blue')

        axs[i].grid(True)

        plt.sca(axs[i])
        plt.yticks([0, 0.5, 1])
        if i == len((data)) - 1:
            axs[i].set_xlabel(r'Frequenz [MHz]', size=45)
    plt.savefig('results/Zyklotronfrequenz.pdf', format='PDF')
    #plt.show()
    plt.clf()
    k = 1
    plt.title('I={0}, U={1}'.format(I[k], V[k]))
    plt.plot(data[k]['f'], data[k]['A']/(np.amax(data[k]['A'])), marker=None, ls='solid', color='darkred')
    plt.grid(True)
    #plt.show()
################
# FullRangeScan#
################
data_456_10_8 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_17_12_17_06_45.6V_1.0A_1-500MHz_8dBm.csv', 0)
data_456_10_15 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_18_18_30_06_45.6V_1.0A_1-500MHz_15dBm.csv', 0)
data_446_11_8 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_15_14_57_27_44.6V_1.1A_1-500MHz_8dBm.csv', 0)
data_456_11_8 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_15_22_30_47_45.6V_1.1A_1-500MHz_8dBm.csv', 0)
data_466_11_8 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_16_06_03_58_46.6V_1.1A_1-500MHz_8dBm.csv', 0)
data_446_11_15 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_16_13_37_17_44.6V_1.1A_1-500MHz_15dBm.csv', 0)
data_456_11_15 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_19_02_03_17_45.6V_1.1A_1-500MHz_15dBm.csv', 0)
data_466_11_15 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_17_04_43_46_46.6V_1.1A_1-500MHz_15dBm.csv', 0)
data_456_12_8 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_18_03_23_36_45.6V_1.2A_1-500MHz_8dBm.csv', 0)
data_456_13_8 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_18_10_56_47_45.6V_1.3A_1-500MHz_8dBm.csv', 0)
data_456_12_15 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_19_09_36_36_45.6V_1.2A_1-500MHz_15dBm.csv', 0)
data_456_13_15 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_19_17_09_47_45.6V_1.3A_1-500MHz_15dBm.csv', 0)
#data_456_11_15 = read_data('FP47_Jasper_Vincent/2022_02_15_14_57_26_FullRangeScan/2022_02_16_21_10_28_45.6V_1.1A_1-500MHz_15dBm.csv', 0)

data = [data_456_10_8, data_456_10_15, data_446_11_8, data_456_11_8, data_466_11_8, data_446_11_15, data_456_11_15, data_466_11_15, data_456_12_8, data_456_13_8, data_456_12_15, data_456_13_15]
current = [1, 1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.1, 1.2, 1.3, 1.2, 1.3]
voltage = [45.6, 45.6, 44.6, 45.6, 46.6, 44.6, 45.6, 46.6, 45.6, 45.6, 45.6, 45.6]
f_minus_exp = unp.uarray([9, 9.3, 8, 8.2, 8.37, 8.04, 8.2, 8.4, 7.6, 7, 7.56, 7.01] , [0.1, 0.8, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.2, 0.17, 0.22, 0.26])
f_z_exp = unp.uarray([76, 76, 75, 75.5, 76.6, 75, 74.8, 74.7, 75.8, 74.2, 75.8, 74.4] , [1, 1, 2, 1.5, 0.8, 1, 2, 3, 1.7, 2, 1.6, 2.6])
f_plus_exp = unp.uarray([328, 328, 364, 364.2, 364, 365, 365, 364.8 , 400, 435.4, 400, 435.6] , [1, 1, 1, 1.5, 1.2, 2, 2, 1.8, 1.2, 1.1, 1.4, 1.6])
f_c_exp = unp.uarray([338, 338, 374, 373.5, 373.3, 373.9, 373.7, 373.4, 408.8, 443.8, 408.8, 444.02] , [1, 1, 1.3, 1.4, 1.4, 1.7, 1.7, 1.5, 1, 1.1, 1.4, 1.1])
frequ = np.array([f_c_exp, f_z_exp, f_minus_exp, f_plus_exp])
#plot_data(data, current, voltage, frequ)

##########################################
#Calculations for the following exercises#
##########################################

####
#12#
####
f_c_calc = f_plus_exp + f_minus_exp
f_c_calc_2 = sqrt(f_plus_exp**2 + f_minus_exp**2 + f_z_exp**2)
print('\n 12. Berechnung der Zyklotronfrequenz aus den Messwerten:')
print('Gemessen:\n' + str(f_c_exp))
print('Berechnet:\n' + str(f_c_calc))
print('Berechnet:\n' + str(f_c_calc_2))
print('Sigma:\n' + str(calc_sigma(f_c_calc, f_c_exp)))
print('Sigma:\n' + str(calc_sigma(f_c_calc_2, f_c_exp)))

####
#13#
####
x_values = np.array([44.6, 45.6, 46.6])
y_44_6 = sum(unp.uarray([75, 75], [1, 2])) / 2
y_45_6 = sum(unp.uarray([76, 76, 75.5, 74.8, 75.8, 74.2, 75.8, 74.4], [1, 1, 1.5, 2, 1.7, 2, 1.6, 2.6])) / 8
y_46_6 = sum(unp.uarray([74.7, 76.6], [3, 0.8])) / 2
y_values = 2 * const.pi * unp.uarray([unp.nominal_values(y_44_6), unp.nominal_values(y_45_6), unp.nominal_values(y_46_6)], [unp.std_devs(y_44_6), unp.std_devs(y_45_6), unp.std_devs(y_46_6)])


popt, pcov = curve_fit(axial_frequ_fit, x_values, unp.nominal_values(y_values), sigma=unp.std_devs(y_values))
chi_ndf, prob = chisquareValue(x_values, unp.nominal_values(y_values), unp.std_devs(y_values), popt, axial_frequ_fit)
plt.clf()
plt.style.use('classic')
plt.rcParams["font.family"]='serif'
plt.rcParams["figure.figsize"][0] = 14
plt.rcParams["figure.figsize"][1] = 9
plt.rcParams['errorbar.capsize']=2

plt.errorbar(x_values, unp.nominal_values(y_values), yerr=unp.std_devs(y_values), marker='.', linewidth=0, color='darkred', label='Messwerte')
plt.plot(x_values, axial_frequ_fit(x_values, *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsfit')

plt.text(45, 462, r'$\chi$ / ndf = {0}, P = {1} %'.format(round(chi_ndf, 2), round(prob, 2)), style='italic', bbox={'facecolor': 'gray', 'alpha': 0.2, 'pad': 5})
plt.text(45, 465, r'$f(x) = a\sqrt{x} + b$,  a = ' + str(round(popt[0], 2)) + ' [e+6/(sV)]', style='italic', bbox={'facecolor': 'gray', 'alpha': 0.2, 'pad': 5})
plt.title(r'Bestimmung des $C_2$-Koeffizienten', size=20)
plt.xlabel('Ringspannung [V]', size=15)
plt.ylabel(r'$\omega_z$ [$MHz$]', size=15)

#plt.tight_layout()
plt.grid(True)
plt.legend(frameon=True, loc='best')
plt.savefig('results/C2Koeffizient.pdf', format='PDF')
#plt.show()


C2_lit = 1.4e+4
C_2_exp = (unc.ufloat(popt[0], np.sqrt(pcov[0,0]))*10**6)**2 * const.m_e / const.elementary_charge / 2
print('\n Bestimmung des C2-Koeffizienten')
print(unc.ufloat(popt[0], np.sqrt(pcov[0,0])))
print('C2_exp: ' + str(C_2_exp))
print('Sigma: '+str(calc_sigma(C2_lit, C_2_exp)))

####
#14#
####
x_values = np.array([1, 1.1, 1.2, 1.3])
y_10 = sum(unp.uarray([338], [1]))
y_11 = sum(unp.uarray([338, 374, 373.5, 373.3, 373.9, 373.7, 373.4], [1, 1.3, 1.4, 1.4, 1.7, 1.7, 1.5])) / 7
y_12 = sum(unp.uarray([408.8, 408.8], [1, 1.4])) / 2
y_13 = sum(unp.uarray([443.8, 444.02], [1.1, 1.1])) / 2
y_values = 2 * const.pi * unp.uarray([unp.nominal_values(y_10), unp.nominal_values(y_11), unp.nominal_values(y_12), unp.nominal_values(y_13)], [unp.std_devs(y_10), unp.std_devs(y_11), unp.std_devs(y_12), unp.std_devs(y_13)])

popt, pcov = curve_fit(linear_fit, x_values, unp.nominal_values(y_values), sigma=unp.std_devs(y_values))
chi_ndf, prob = chisquareValue(x_values, unp.nominal_values(y_values), unp.std_devs(y_values), popt, linear_fit)
plt.clf()

plt.errorbar(x_values, unp.nominal_values(y_values), yerr=unp.std_devs(y_values), marker='.', linewidth=0, color='darkred', label='Messwerte')
plt.plot(x_values, linear_fit(x_values, *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsfit')

plt.text(1.1, 2150, r'$f(x) = ax$,  a = ' + str(round(popt[0], 2)) + r' [$\frac{MHz}{A}$]', style='italic', bbox={'facecolor': 'gray', 'alpha': 0.2, 'pad': 5})
plt.text(1.1, 2050, r'$\chi$ / ndf = {0}, P = {1} %'.format(round(chi_ndf, 2), round(prob, 2)), style='italic', bbox={'facecolor': 'gray', 'alpha': 0.2, 'pad': 5})

plt.title(r'Bestimmung des Windungszahl N', size=20)
plt.xlabel('Magnetfeldstromstärke [A]', size=15)
plt.ylabel(r'$\omega_c$ [$MHz$]', size=15)
plt.xlim(0.90, 1.4)
plt.grid(True)
plt.legend(frameon=True, loc='best')
#plt.savefig('results/N_Bestimmung.pdf', format='PDF')
#plt.show()

a = unc.ufloat(popt[0], np.sqrt(pcov[0, 0])) * 10**6
L = unc.ufloat(246, 0.5) * const.milli
N = a * L * const.m_e / const.elementary_charge / const.mu_0
N_theo = unc.ufloat(2400, 30)
print('\n Teil 14: Bestimmung der Windungszahl')
print('N experimentell ist ' + str(N))
print('N theo ist ' + str(N_theo))
print('Sigma ' + str(calc_sigma(N_theo, N)))

####
#15#
####
ratio = a * L / const.mu_0 / N_theo
e_mass = const.elementary_charge / ratio

print('\n15. Bestimmung der Elektronenmasse')
print('Relative Verhältnis ' + str(ratio))
print(const.elementary_charge)
print('Elektronenmasse experimentell ' + str(e_mass))
print('Elektronenmasse experimentell ' + str(const.m_e))
print('Sigma ' + str(calc_sigma(const.m_e, e_mass)))