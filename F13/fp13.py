import scipy.constants as const
import numpy as np
import uncertainties as unc
import uncertainties.unumpy as unp
from uncertainties.umath import sqrt

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

tau_0 = unc.ufloat(2200, 100) * const.nano
h_quer = const.h / (2*const.pi)
muon_g = unc.ufloat(const.value('muon g factor'), 13e-12)
muon_mag_moment = unc.ufloat(const.value('muon mag. mom.'), 1e-34)
B = unc.ufloat(20*2e-4, 0.02*2e-4)#T
w_L = 2 * muon_mag_moment * B / h_quer
G = sqrt((192*const.pi**3*h_quer)/(tau_0*(const.value('muon mass energy equivalent'))**5))*const.e**2*1e18 #natural units
G_lit = unc.ufloat(const.value('Fermi coupling constant'), const.value('Fermi coupling constant')*5.1e-7) #Gev^-2

omega_L_exp = unc.ufloat(0.003398, 0.000085)
w_L = (-1)*w_L*1e-9
print(w_L)
print(calc_sigma(w_L, omega_L_exp))
omega_L_exp = unc.ufloat(0.003583, 0.000044)
print(calc_sigma(w_L, omega_L_exp))
PA = unc.ufloat(0.0669, 0.0064)
print(calc_sigma(PA, 0.759))

