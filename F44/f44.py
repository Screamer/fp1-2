import os

import scipy.constants as const
import numpy as np
import uncertainties as unc
import uncertainties.unumpy as unp
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as stats
from scipy.signal import argrelmin, argrelmax
from scipy.special import voigt_profile
from scipy.interpolate import interp1d
from uncertainties.unumpy import sqrt, arctan

plt.style.use('classic')
plt.rcParams["font.family"]='serif'

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

def chisquareValue(xvalue, yvalues, sigma_y, fit_parameter, function):
    chi2=0
    for i in range(len(xvalue)):
        expected = function(xvalue[i], *fit_parameter)
        chi2+=((expected-yvalues[i])/sigma_y[i])**2
    prob = round(1 - stats.chi2.cdf(chi2, len(xvalue) - len(fit_parameter)), 4) * 100
    return chi2/(len(xvalue)-len(fit_parameter)), prob #chis2 per degree of freedom and probability


def Gauss(x, A, mu, sigma):
    return A/(np.sqrt(2*np.pi)*sigma)*np.exp(-(x-mu)**2/2/sigma**2)

def TripleGauss(x, N1, N2, N3, gamma1, gamma2, gamma3, sigma1, sigma2, sigma3):
    return N1*np.exp(-((x-gamma1)**2/(sigma1**2))) + N2*np.exp(-((x-gamma2)**2/(sigma2**2))) + N3*np.exp(-((x-gamma3)**2/(sigma3**2)))

def Quadratic(x, C1, C2, C3):
    return C1*x**2 + C2*x + C3

def starting_values(intensity, pixel, order = 7):
    values = []
    maxima = argrelmax(intensity, order = order)[0]
    if len(maxima) > 3:
        maxima = maxima[:3]
    int_max = intensity[maxima]
    mu = 20 * np.ones(3)
    values.extend(int_max)
    values.extend(pixel[maxima])
    values.extend(mu)
    return values

def linear_fit(x,a,b):
    return a*x+b

def calc_Da(C1, C2, C3, dC1, dC2, dC3):
    a1 = unc.ufloat(C1, dC1)
    a2 = unc.ufloat(C2, dC2)
    a3 = unc.ufloat(C3, dC3)
    return -a2/(2*a1)+sqrt(a2**2/(4*a1**2)+1-a3)
###########
#Hysterese#
###########
current = np.array([0,2,4,6,8,10,12,13])
current_err = 0.1*np.ones(8)

magnetic_up = np.array([[0.002,0.002,0.001],[0.126,0.127,0.126],[0.257,0.268,0.250],[0.373,0.374,0.368],[0.475,0.481,0.474],[0.582,0.589,0.584],[0.668,0.690,0.673],[0.711,0.715,0.710]])
magnetic_up_err = magnetic_up*0.02
mag_up = np.mean(magnetic_up,axis=1)
mag_up_err = np.sqrt(np.mean(magnetic_up_err, axis=1)*1/3)

magnetic_down = np.array([[0.002,0.002,0.002],[0.218,0.125,0.130],[0.254,0.249,0.246],[0.380,0.375,0.374],[0.5,0.490,0.495],[0.590,0.602,0.595],[0.681,0.679,0.674],[0.708,0.713,0.705]])
magnetic_down_err = magnetic_down*0.02
mag_down = np.mean(magnetic_down,1)
mag_down_err = np.sqrt(np.mean(magnetic_down_err,1)*1/3)

popt_up, pcov_up = curve_fit(linear_fit, current, mag_up, sigma=mag_up_err)
popt_down, pcov_down = curve_fit(linear_fit, current, mag_down, sigma=mag_down_err)
chi_up, p_up = chisquareValue(current, mag_up, mag_up_err, popt_up, linear_fit)
chi_down, p_down = chisquareValue(current, mag_down, mag_down_err, popt_down, linear_fit)

x = np.linspace(0, 13, 2)
a_1 = r'$a_1 = ({0}\pm{1})$ mT/A'.format(round(popt_up[0],3), round(np.sqrt(pcov_up[0, 0]), 3))
a_2 = r'$a_2 = ({0}\pm{1})$ mT/A'.format(round(popt_down[0],3), round(np.sqrt(pcov_up[0, 0]), 3))

plt.clf()

plt.errorbar(current, mag_up, xerr=current_err, yerr=mag_up_err, ls='', fmt='.', color='darkred', label='Messwerte aufsteigend')
plt.errorbar(current, mag_down, xerr=current_err, yerr=mag_down_err, ls='', fmt='.', color='darkblue', label='Messwerte absteigend')

plt.plot(x, linear_fit(x, *popt_up), marker='None', ls='--', color='darkred', label=r'$a_1x+b_1$')
plt.plot(x, linear_fit(x, *popt_down), marker='None', ls='--', color='darkblue', label=r'$a_2x+b_2$')

plt.text(5,0.05,'Fitparameter: \n {0} \n {1}'.format(a_1, a_2),
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':10},
        fontsize=15)

plt.title('Hysterese', size=20)
plt.xlabel('Stromstärke I [A]', size=15)
plt.ylabel('Feldstärke B [mT]', size=15)
plt.grid(True)
plt.legend(frameon=True, loc='best')
#plt.savefig('results/hysterese.pdf', format='PDF')
plt.show()

####################
#Increasing current#
####################
n = unc.ufloat(1.4567, 1.4567*0.005)
d = unc.ufloat(4.04e-3, 4.04e-3*0.005)

#Lambda ersetzen
lam = unc.ufloat(643.89e-9, 0.15e-9)

all_data = []
mu_res = []
dmu_res = []
dE = []
B_res = []
b1 = unc.ufloat(popt_up[0], np.sqrt(pcov_up[0,0]))
b2 = unc.ufloat(popt_up[1], np.sqrt(pcov_up[1,1]))
for file in os.listdir('data/teil1/'):
    if file.startswith('t') and file.endswith('.txt'):
        print(file)
        data = pd.read_csv('data/teil1/'+file, sep='\s+')
        A = file[2:4]
        #A_res.append(int(A))
        data.columns = ['x', 'I']

        all_data.append(data)

        pixels = data['x'][:-1].to_numpy()
        intensity = (data['I'][:-1]/(-np.amin(data['I'][:-1]))+1).to_numpy()
        intensity = (intensity / np.amax(intensity[:-1]))

        minimum = argrelmin(intensity, order=40)[0]
        pixels_min = pixels[minimum]
        intensity_min = intensity[minimum]

        if A == '11' or A == '12' or A == '10':
            num_min = len(minimum) - 2
            k = 2
        else:
            num_min = len(minimum) - 1
            k = 1

        f = interp1d(pixels_min, intensity_min, kind='quadratic')
        pixelrange = np.arange(pixels_min[0], pixels_min[num_min])
        filtered_intensity = intensity[minimum[0]:minimum[num_min]] - f(pixelrange)
        filtered_intensity = filtered_intensity / np.amax(filtered_intensity)

        popt_all = []
        pcov_all = []

        plt.clf()

        for i in range(0, num_min-1):
            x_intervall = pixels[minimum[i]:minimum[i+1]]
            y_intervall = filtered_intensity[minimum[i]:minimum[i+1]]

            N1 = np.amax(y_intervall)
            gamma1 = np.where(y_intervall == np.amax(y_intervall))[0][0]
            p0 = starting_values(y_intervall, x_intervall, order=5)

            popt, pcov = curve_fit(TripleGauss, x_intervall, y_intervall, p0=p0, maxfev=60000)
            #chi, prob = chisquareValue(x_intervall, y_intervall, np.sqrt(y_intervall), popt, TripleGauss)

            pcov_all.append(pcov)
            popt_all.append(popt)
            plt.plot(x_intervall, TripleGauss(x_intervall, *popt), ls='--', lw=1, marker=None, color='darkred')
            plt.plot(x_intervall, y_intervall, color='darkblue', linewidth=0, marker='.')

        plt.title(r'Zeeman-Effekt: $\sigma$- und $\pi$-Linien, I={0}A'.format(A), size=20)
        plt.xlabel('Pixel [px]', size=15)
        plt.ylabel('Intensität', size=15)
        plt.grid(True)
        plt.savefig('results/'+file+'.pdf', format='PDF')
        #plt.show()

        plt.clf()
        plt.title(r'Position der $\pi$-Linie für I={0}A'.format(A), size=20)
        plt.xlabel('Pixel [px]', size=15)
        plt.ylabel('Interferenzordnung k', size=15)

        order = np.arange(0, len(popt_all))
        pos = []
        sig = []
        k1 = []
        dk1 = []
        k2 = []
        dk2 = []
        k3 = []
        dk3 = []
        for i in popt_all:
            pos.append(i[4])
            sig.append(i[7])

            k1.append(i[3])
            k2.append(i[4])
            k3.append(i[5])
            dk1.append(i[6])
            dk2.append(i[7])
            dk3.append(i[8])
        popt, pcov = curve_fit(Quadratic, pos, np.arange(0, len(pos)), sigma=sig)
        chi, prob = chisquareValue(pos, np.arange(0, len(pos)), np.ones(len(pos))*0.02, popt, Quadratic)
        #print(chi)
        plt.errorbar(pos, np.arange(0, len(pos)), xerr=sig, fmt='.', color='darkred',label='Messwerte')
        plt.plot(np.linspace(0, pos[len(pos) - 1],10), Quadratic(np.linspace(0, pos[len(pos) - 1],10), *popt), marker=None, ls='--', color='darkblue', label=r'$\sum_{i=0}^{2}\lambda_ix^i$')
        c1 = r'$C_1$={0}$\pm${1}'.format(round(popt[0],4), round(np.sqrt(pcov[0,0]), 6))
        c2 = r'$C_2$={0}$\pm${1}'.format(round(popt[1], 4), round(np.sqrt(pcov[1, 1]), 6))
        c3 = r'$C_3$={0}$\pm${1}'.format(round(popt[2], 4), round(np.sqrt(pcov[2, 2]), 4))
        plt.text(40, 4.75, 'Fitparameter: \n {0} \n {1} \n {2}'.format(c1, c2, c3),
                 bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10},
                 fontsize=15)
        plt.grid(True)
        plt.xlim(0, np.amax(pos)+20)
        plt.ylim(-1, len(popt_all)+1)
        plt.legend(frameon=True, loc='upper right')
        #plt.savefig('results/' + file + '_linear.pdf', format='PDF')
        #plt.show()

        #Magneton berechnen
        B = linear_fit(int(A), b1, b2)
        B_res.append(B)
        k1 = unp.uarray(k1, np.abs(dk1))
        k2 = unp.uarray(k2, np.abs(dk2))
        k3 = unp.uarray(k3, np.abs(dk3))
        c1 = unc.ufloat(popt[0], np.sqrt(pcov[0,0]))
        c2 = unc.ufloat(popt[1], np.sqrt(pcov[1,1]))
        c3 = unc.ufloat(popt[2], np.sqrt(pcov[2,2]))
        Da = 1
        dk1 = sum(Quadratic(k2, c1, c2, c3) - Quadratic(k1, c1, c2, c3))/len(k2)
        dk2 = sum(Quadratic(k3, *popt) - Quadratic(k2, *popt))/len(k2)
        dk = (dk1+dk2)/2
        mu_b = const.c * const.h / B * dk / (2*d*sqrt(n**2-1))
        print('Ergebnis\n')
        print('Mu={0}'.format(mu_b))
        print('Sigma={0}'.format(calc_sigma(mu_b, const.value('Bohr magneton'))))
        print(B)
        print('\n')

        mu_res.append(unp.nominal_values(mu_b))
        dmu_res.append(unp.std_devs(mu_b))

        Da = calc_Da(*popt, np.sqrt(pcov[0, 0]), np.sqrt(pcov[1, 1]), np.sqrt(pcov[2, 2]))
        dk12 = sum(k2-k1)/len(k2-k1)
        dk23 = sum(k3-k2)/len(k3-k2)
        da_mean = (dk12+dk23) / 2
        ratio = da_mean/Da
        dlam = ratio * lam**2 /(2*d*sqrt(n**2-1))

        dE.append(mu_b * B)
        #dE.append(const.c * const.h * dlam / (lam**2))
        #dE.append(ratio * (2*d*sqrt(n**2-1))*1e-19)

        # mu_b = const.h * const.c / B * ratio * 10**3
        # print(da_mean/Da)
        # print(mu_b)

mu = unp.uarray(mu_res, dmu_res)
# print(sum(mu)/len(mu))
# print(calc_sigma(sum(mu)/len(mu), const.value('Bohr magneton')))

popt, pcov =curve_fit(linear_fit, unp.nominal_values(B_res), unp.nominal_values(dE), sigma=unp.std_devs(dE))
res = (unp.nominal_values(dE) - linear_fit(unp.nominal_values(B_res), *popt)) / np.abs(unp.std_devs(dE))
plt.clf()
fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True)

axs[0].errorbar(unp.nominal_values(B_res), unp.nominal_values(dE), xerr=unp.std_devs(B_res), yerr=unp.std_devs(dE), lw=0, fmt='.', color='darkred', label='Messwerte')
axs[0].plot(unp.nominal_values(B_res), linear_fit(unp.nominal_values(B_res), *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
axs[0].grid(True)
axs[1].grid(True)
axs[0].legend(frameon=True, loc='upper left', fontsize=10)
axs[0].set_ylabel(r'$\Delta E$ [J]', size=15)
axs[0].text(0.675, 2.5*1e-24, 'Fitparameter: \n m={0} \n b={1}'.format(unc.ufloat(popt[0], np.sqrt(pcov[0,0])),unc.ufloat(popt[1], np.sqrt(pcov[1,1]))),
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':10},
        fontsize=10)
axs[1].set_ylabel(r'Abweichung [$\sigma$]', size=15)
axs[1].errorbar(unp.nominal_values(B_res), res, xerr=0, yerr=0, marker='.', lw=0, color='darkred')
axs[1].plot(np.linspace(0.45, 0.8, 2), np.linspace(0.45, 0.8, 2) * 0, ls='--', marker=None, color='darkblue')
axs[1].set_ylim(-1.2, 1.2)

fig.suptitle(r'$\Delta E$ in Abhängigkeit von B', size=20)
axs[1].set_xlabel(r'B [mT]', size=15)
axs[0].set_xlabel(r'B [mT]', size=15)
#plt.savefig('results/Ausgleichsgerade.pdf', format='PDF')
#plt.show()
print(pcov)
print(calc_sigma(const.value('Bohr magneton'), unc.ufloat(popt[0], np.sqrt(pcov[0,0]))))
#########
##Teil 2#
#########
plt.clf()
fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True)
#fig.subplots_adjust(hspace=0)
data = pd.read_csv('data/teil2/ne_lines_4.txt', sep='\s+')
data.columns = ['x', 'I']

pixels = data['x'][:-1].to_numpy()
intensity = (data['I'][:-1] / (-np.amin(data['I'][:-1])) + 1).to_numpy()
intensity = (intensity / np.amax(intensity[:-1]))

max = argrelmax(intensity, order=50)

axs[0].errorbar(pixels, intensity, lw=0, fmt='.', color='darkred', label='Messwerte')

mu_res = []
sig_res = []
wave = [630.47893, 633.44276, 638.29914, 640.2248]
k = 0
for i in max[0]:
    i_start = i - 60
    i_stop = i + 60
    #plt.axvspan(pixels[i_start],pixels[i_stop], alpha=0.5, color='red') Debugging
    popt, pcov = curve_fit(Gauss, pixels[i_start:i_stop], intensity[i_start:i_stop], p0=[intensity[i], i, 50], maxfev=6000)
    chi, prob = chisquareValue(pixels[i_start:i_stop], intensity[i_start:i_stop], np.sqrt(intensity[i_start:i_stop]), popt, Gauss)
    axs[0].vlines(i, 0, intensity[i], color='black')
    mu = unc.ufloat(round(popt[1], 2), round(np.sqrt(pcov[1, 1]),2))
    sig = unc.ufloat(round(popt[2], 2), round(np.sqrt(pcov[2, 2]), 2))
    axs[0].plot(pixels[i_start:i_stop], Gauss(pixels[i_start:i_stop], *popt), ls='--', color='darkblue', label=r'Gauss, $\mu=${0} $\sigma=${1}'.format(mu, sig))
    if k < 4 : axs[0].text(i - 50, intensity[i]+0.05, r'$\lambda$={}nm'.format(round(wave[k], 1)), fontsize=10, color='black')
    mu = unc.ufloat(popt[1], np.sqrt(pcov[1, 1]))
    sig = unc.ufloat(popt[2], np.sqrt(pcov[2, 2]))
    mu_res.append(mu)
    sig_res.append(sig)
    k=k+1

axs[0].errorbar(pixels[max[0][4]-50:max[0][4]+50], 20*intensity[max[0][4]-50:max[0][4]+50], lw=0, fmt='.', color='magenta', alpha=0.5)
popt, pcov = curve_fit(linear_fit, wave, unp.nominal_values(mu_res[:-1]), sigma=unp.nominal_values(sig_res[:-1]))
chi, prop = chisquareValue(wave, unp.nominal_values(mu_res[:-1]), unp.nominal_values(sig_res[:-1]), popt, linear_fit)
a = unc.ufloat(popt[0], np.sqrt(pcov[0,0]))
b = unc.ufloat(popt[1], np.sqrt(pcov[1,1]))
for i in range(0,len(mu_res) - 1):
    axs[1].errorbar(unp.nominal_values(mu_res[i]), wave[i], xerr=unp.nominal_values(sig), lw=0, fmt='.', color='darkred', label='Extrema')

axs[1].plot(unp.nominal_values(mu_res), linear_fit(unp.nominal_values(mu_res), 1/unp.nominal_values(a), -popt[1]/popt[0]), marker=None, ls='--', color='darkblue')

x = unc.ufloat(unp.nominal_values(mu_res[len(mu_res)-1]), unp.nominal_values(sig_res[len(sig_res)-1]))
lam = linear_fit(x, 1/a, -b/a)

plt.text(700,632,'Fitparameter: \n m={0} \n b={1}'.format(1/a, -b/a),
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':10},
        fontsize=15)
plt.text(100,640,r'$\lambda$={0} nm'.format(lam),
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':10},
        fontsize=15)
fig.suptitle('Charakteristische Neonspektrum', size=20)
axs[0].set_ylabel('normierte Intensität', size=15)
axs[1].set_ylabel('Wellenlänge [nm]', size=15)
axs[1].set_xlabel('Pixel [px]', size=15)
axs[0].grid(True)
axs[1].grid(True)
plt.savefig('results/NeonSpektrum.pdf', format='PDF')
#plt.show()

#############
#Ne-Spektrum#
#############
spec_x = []
spec_y = []
ne_4 = pd.read_csv('data/teil2/ne_1.txt', sep='\s+')[:-1]
ne_3 = pd.read_csv('data/teil2/ne_2.txt', sep='\s+')[:-1]
ne_2 = pd.read_csv('data/teil2/ne_3.txt', sep='\s+')[:-1]
ne_1 = pd.read_csv('data/teil2/ne_4.txt', sep='\s+')[:-1]
data = [[ne_1, ne_2], [ne_3, ne_4]]
plt.clf()
fig, axs = plt.subplots(nrows=2, ncols=2)
add = 0
for i in range(0,2):
    for j in range(0, 2):
        data[i][j].columns = ['x', 'I']
        x_data = data[i][j]['x']
        y_data = data[i][j]['I']

        y_data = (y_data / (-np.amin(y_data)) + 1).to_numpy()
        y_data = (y_data / np.amax(y_data))

        max = argrelmax(y_data, order=140)
        #print(max)
        for k in max[0]:
            axs[i][j].vlines(k - max[0][0], 0, y_data[k], color='darkblue')
        axs[i][j].grid(True)
        axs[i][j].errorbar(x_data - max[0][0], y_data, color='darkred', lw=0, fmt='.')
        spec_x.append(data[i][j]['x'] - max[0][0] + add)
        spec_y.append(y_data)
        add = add + max[0][len(max[0])-1] - max[0][0]

#plt.show()
plt.clf()
for i in range(0, len(spec_x)):
    plt.plot(spec_x[i], spec_y[i], lw=2)

plt.title('Zusammengesetztes Neonspektrum', size=20)
plt.ylabel('normierte Intensität', size=15)
plt.xlabel('Pixel [px]', size=15)
plt.grid(True)
plt.ylim(0, 1.1)
plt.xlim(0, 5000)
plt.savefig('results/Neonspektrum_gesamt.pdf', format='PDF')
#plt.show()