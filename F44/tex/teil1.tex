\section{Zeemannspektroskopie}
Die Dipolstrahlung einer Cadiumlampe resultiert aus dem Übergang $^1D_2 \rightarrow ^1P_1$. Bevor der Zeeman-Effekt untersucht wurde, musste die Signifikanz der Magnetfeldhysterese determiniert werden.
Die Stromstärke der Spule wurde in regelmäßigen Schritten verringert bzw. verstärkt und mithilfe eines Hall-Sensors wurde dreimal die Magnetfeldstärke bestimmt und über diese gemittelt.
\begin{minipage}{\columnwidth}
    \centering
    \includegraphics[width=0.8\columnwidth]{../results/hysterese.pdf}
    \figcaption{Magnetfeldstärke für aufsteigende und absteigende Stromstärke}
    \label{fig:1}
\end{minipage}
\noindent
Anhand Abbildung \ref{fig:1} ist eindeutig erkennbar, dass der Hysterese-Effekt vernachlässigt werden kann.
\newline\break
Danach wurde der Zeeman-Effekt in transversaler als auch longitudinaler Richtung untersucht.
Betrachtet man entlang des Magnetfeldes, wird man die $\Delta m = \pm 1$ Linien erkennen können, welche rechts/links zirkular (oder elliptisch) polarisiert sind (Abbildung \ref{fig:lg_ohne}).
Anwenden einer $\lambda / 4$ Platte linearpolarisiert diese und hinzufügen eines Polarisationsfilters erlaubt das Filtern einer bestimmten Linie (Abbildung \ref{fig2}).
\noindent
\begin{minipage}{\columnwidth}
  \makeatletter
  \newcommand{\@captype}{figure}
  \makeatother
  \centering
  \subfloat[$\pm\sigma$ ohne Filter]{%
    \includegraphics[width=0.4\columnwidth]{../data/teil1/lg_ohne_Filter.png}%
    \label{fig:lg_ohne}%
  }\qquad%
  \subfloat[$\pm\sigma$ mit $\lambda / 4$- und Polarisationsfilter]{%
    \includegraphics[width=0.4\columnwidth]{../data/teil1/lg_Lambda_Linear_Filter2.png}%
    \label{fig2}%
  }
  \captionof{figure}{Zeeman-Effekt entlang des Magnetfelds}
\end{minipage}
\\\break
Senkrecht zum Magnetfeld sind die $\Delta m = 0,\pm 1$ Linien als linear erkennbar, wobei die $\Delta m=0$ Linie parallel zum Magnetfeld ist. Daher übt die $\lambda / 4$ Platte keinen sichtbaren Einfluss aus,
wohingegen der Polarisationsfilter das Filtern der $\Delta m=0,\pm 1$ (Das Vorzeichen $\Delta m$ ist abhängig von der Betrachtungsachse \ref{fig:zeeman}) Linien erlaubt (Abbildung \ref{fig3}).

\noindent
In transversaler Richtung wurden die erhaltenen Abbildungen für verschieden starke Magnetfelder entlang der Strichlinie den Intensitäten gemäß komprimiert. 
Aus diesen Messdaten wurden anhand einer Voigt-Funktion, d.h. einer Faltung aus Gauß und Breit-Wigner, für jede Ordnung das Profil gefittet.
\noindent
\begin{minipage}{\columnwidth}
  \makeatletter
  \newcommand{\@captype}{figure}
  \makeatother
  \centering
  \subfloat[$\pm\sigma,\pi$ ohne Filter]{%
    \includegraphics[width=0.4\columnwidth]{../data/teil1/trans_ohne_Filter.png}%
    \label{fig:tr_ohne}%
  }\qquad%
  \subfloat[$\pm\sigma,\pi$ Polarisationsfilter]{%
    \includegraphics[width=0.4\columnwidth]{../data/teil1/trans_linear_Filter.png}%
    \label{fig3}%
  }
  \captionof{figure}{Zeeman-Effekt orthogonal zum Magnetfeld}
\end{minipage}
\begin{align}
    V(x,\sigma, \gamma) &= (G*L)(x) = \int_\mathbb{R}G(t)L(x-t)dt
\end{align}
\noindent
\begin{center}
    \centering
    \includegraphics[width=0.8\linewidth]{../results/t_13A_17V.txt.pdf}
    \captionof{figure}{normiertes Intensitätsprofil als Funktion der Pixelposition. Extrapoliert durch Voigt-Funktionen. }
    \label{fig4}
\end{center}

\noindent
Anhand der $\pi$-Linienposition lässt sich die Ordnung der $\sigma$-Linien bestimmen, wobei hierfür der Zusammenhang mithilfe eines Polynoms 2ten Grades genähert wurde.
\noindent
\begin{center}
    \centering
    \includegraphics[width=0.8\linewidth]{../results/t_13A_17V.txt_linear.pdf}
    \figcaption{Brechungsordnung in Abhängigkeit der $\pi$-Linienposition extrapoliert mit einem Polynom 2ten Grades}
    \label{fig5}
\end{center}
\noindent
$\Delta k$ bezeichnet die Ordnungsdifferenz der $\pi$- und $\sigma$-Linien und wurde durch gegebenes Polynom (Abbildung \ref{fig5}) berechnet.
Betrachtet man die Lummer-Gehrcke Platte, lässt sich die Wellenlängendifferenz $\delta\lambda$ berechen:
\begin{align}
    \delta\lambda &= \frac{\delta k}{\Delta k}\Delta\lambda = \delta k \Delta\lambda\\
    \Delta\lambda &= \frac{\lambda^2}{2d\sqrt{n^2-1}}
\end{align}
Die Energie der Dipolstrahlung anhand einer Taylor-Reihe erster Ordnung approximiert:
\begin{align}
    \Delta E &= hc(\frac{1}{\lambda + \delta\lambda}-\frac{1}{\lambda})\approx hc\frac{\delta\lambda}{\lambda^2}
\end{align}
Gleichsetzen mit der Zeeman-Energie Formel \ref{eq:zeeman}
\begin{align}
    \mu_B &= \frac{hc}{2Bd\sqrt{n^2-1}}\delta k
\end{align}
\begin{center}
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        B[mT]   &   $\mu_B$ [$10^{-24}$J/T]   &   $\sigma$ \\
        \hline
        $0.456 \pm 0.010$   &   $6.2\pm 1.5$    &   $2$\\
        $0.523 \pm 0.012$   &   $8.7\pm 1.9$    &   0.3\\
        $0.581 \pm 0.012$   &   $10.0 \pm 2.4$  &   0.3\\
        $0.639 \pm 0.013$   &   $7.6 \pm 1.2$   &   1.4\\
        $0.755\pm 0.015$    &   $8.1 \pm 1.0$   &   1.1\\
        \hline
    \end{tabular}
    \captionof{table}{Zusammenfassung der bestimmten Magnetons sowie deren Abweichung zum Literaturwert}
    \label{tab:1}
\end{center}
\noindent
Zum Verlgeich wurde der Literaturwert $\mu_B = 9.27400949\cdot10^{-24}$ J/T verwendet.
Weiterhin lässt sich $\Delta E$ als Funktion von $B$ darstellen (Abbildung \ref{fig:ausgleich})

\begin{minipage}{0.9\columnwidth}
  \centering
  \includegraphics[width=0.8\columnwidth]{../results/Ausgleichsgerade.pdf}
  \figcaption{$\Delta E$ als Funktion von $B$. Darunter die Abweichung zur Ausgleichsgerade.}
  \label{fig:ausgleich}
\end{minipage}
\noindent
Aufgrund Formel \ref{eq:zeeman} ist $\Delta E \propto B$, weshalb eine Ausgleichsgerade gezogen wurde, deren Steigung $m$ dem Bohrschen Magneton entspricht.
Experimentell wurde der Wert $\mu_B = (10.2 \pm 2.7)\cdot10^{-24}$ J/T bestimmt mit einer Abweichung von $0.4\sigma$, was aufgrund des überaus großen Fehlers nicht weiter verwunderlich ist.
Anhand Abbildung \ref{fig:ausgleich} lässt sich auch feststellen, dass die konstante Geradenverschiebung $b$ in derselben Größenordnung ist. Unter Berücksichtigung von Tabelle \ref{tab:1} liegt dies sowohl an der Fluktuation der $\Delta E$-Werte als auch an der ungenauen Magnetfeldbestimmung. 
