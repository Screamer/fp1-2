import matplotlib.pyplot
import pandas as pd
import numpy as np
import pylab as p
from scipy.optimize import curve_fit
from scipy.signal import argrelmax
import matplotlib.pyplot as plt
import scipy.stats as stats
import uncertainties.unumpy as unp
import uncertainties as unc
import scipy.constants as const
import math
from uncertainties.umath import sqrt , cos


def lorentzian(omega, A0, omega0, gamma):
    return A0 / np.sqrt((omega0**2 - omega**2)**2 + gamma**2 * omega**2)


def phi(omega, omega0, gamma, test, offset):
    return np.arctan((gamma * (omega-test)) / (omega0**2 - (omega-test)**2)) + offset

def linear_fit(x, a, b):
    return a * x + b


def residual(values, fitvals, errors, arg = 'Amp', fitfunc = lorentzian):
    residuals = []
    res_corr = []
    frequ_residuals = []
    for i in range(len(values[arg])):
        residuals.append((values[arg][i] - fitvals[i]) / errors[i] )
    for i in range(len(residuals)):
        if np.abs(residuals[i]) < 3:
            res_corr.append(residuals[i])
            frequ_residuals.append(2*const.pi*values['frequ'][i])
    return np.array(res_corr), np.array(frequ_residuals)

def chisquareValue(xvalue, yvalues, sigma_y, fit_parameter, function):
    chi2=0
    for i in range(len(xvalue)):
        expected = function(xvalue[i],*fit_parameter)
        chi2+=((expected-yvalues[i])/sigma_y[i])**2
    prob = round(1 - stats.chi2.cdf(chi2, len(xvalue) - len(fit_parameter)), 4) * 100
    return chi2/(len(xvalue)-len(fit_parameter)), prob #chis2 per degree of freedom and probability

def read_data(filedir, skiprows):
    data = pd.read_csv(filedir, skiprows=skiprows, delimiter='\s+')
    data.columns = ['Amp', 'delta_Amp', 'phi', 'delta_phi', 'frequ']
    return data


def plot_data(values, i_start, i_stop, xlim0, xlim1, filepath, p0=None, phi_start=None, phi_stop=None, i=0, pphi=None):
    #Nötig für Indexverschiebung
    use_frequ = np.array(2*const.pi*values['frequ'][i_start:i_stop])
    use_amp = np.array(values['Amp'][i_start:i_stop])
    #c_n
    c = np.array([-0.327, 0.108, 0.524])

    #####
    #Fit#
    #####
    popt, pcov = curve_fit(lorentzian, 2 * const.pi * values['frequ'][i_start:i_stop], values['Amp'][i_start:i_stop], sigma=values['delta_Amp'][i_start:i_stop], p0=p0)
    popt_phi, pcov_phi = curve_fit(phi, 2 * const.pi * values['frequ'][phi_start:phi_stop], values['phi'][phi_start:phi_stop], sigma=values['delta_phi'][phi_start:phi_stop], p0=pphi, maxfev=50000)
    #print(popt_phi)


    ############
    #Stochastik#
    ############
    chi_ndf, p = chisquareValue(use_frequ, use_amp, np.array(values['delta_Amp'][i_start:i_stop]), popt, lorentzian)
    chi_ndf_phi, p_phi = chisquareValue(2*const.pi*np.array(values['frequ'][phi_start:phi_stop]), np.array(values['phi'][phi_start:phi_stop]), np.array(values['delta_phi'][phi_start:phi_stop]), popt_phi, phi)
    res_corr, frequ_res = residual(values, lorentzian(2*const.pi*values['frequ'], *popt), values['delta_Amp'])
    res_corr_phi, frequ_res_phi = residual(values, phi(2*const.pi*values['frequ'], *popt_phi), values['delta_phi'], arg='phi', fitfunc=phi)


    #########
    #Plotten#
    #########
    plt.clf()
    fig, axs = plt.subplots(nrows=4, ncols=1, sharex=True)
    fig.subplots_adjust(hspace=0)

    axs[0].errorbar(2*const.pi*values['frequ'], values['Amp'] / np.amax(lorentzian(2*const.pi*values['frequ'], *popt)), yerr=values['delta_Amp'], linewidth=0, marker='.', color='purple', label='Messwerte')
    axs[0].errorbar(2*const.pi*values['frequ'][i_start:i_stop], values['Amp'][i_start:i_stop] / np.amax(lorentzian(2*const.pi*values['frequ'], *popt)), yerr=values['delta_Amp'][i_start:i_stop], linewidth=0, marker='.', color='darkred', label='Fitwerte')
    axs[0].plot(2*const.pi*values['frequ'], lorentzian(2*const.pi*values['frequ'], *popt) / np.amax(lorentzian(2*const.pi*values['frequ'], *popt)), ls='--', marker=None, color='darkblue', label='Ausgleichsfunktion')
    axs[0].vlines(popt[1], 0, 1, color='gray', label=r'$\omega_0 =$ {} [1/s]'.format(round(popt[1], 2)))
    axs[0].text(0.95, 0.8, r'$\chi$ / ndf = {0}'.format(round(chi_ndf, 2)), style='italic', bbox={'facecolor': 'gray', 'alpha' : 0.3, 'pad' : 10}, transform=axs[0].transAxes, horizontalalignment='right', verticalalignment='top')
    axs[0].set_ylim(0, 1.1)

    axs[1].plot(np.linspace(2*const.pi*values['frequ'][0], 2*const.pi*values['frequ'][len(values['frequ']) - 1], 2), np.zeros(2), color='darkblue', marker=None, ls='solid', linewidth=3)
    axs[1].plot(frequ_res, res_corr, marker='.', linewidth=0, color='darkred', label='Residuen')

    axs[2].errorbar(2*const.pi*values['frequ'], values['phi'], yerr=values['delta_phi'], marker='.', linewidth=0, color='purple', label='Messwerte')
    axs[2].errorbar(2*const.pi*values['frequ'][phi_start:phi_stop], values['phi'][phi_start:phi_stop], yerr=values['delta_phi'][phi_start:phi_stop], marker='.', markersize=6, linewidth=0, color='darkred', label='Fitwerte')
    axs[2].plot(2*const.pi*values['frequ'], phi(2*const.pi*values['frequ'], *popt_phi), marker=None, ls='--', color='darkblue', label='Ausgleichsfunktion')
    axs[2].text(0.95, 0.2, r'$\chi$ / ndf = {0}'.format(round(chi_ndf_phi, 2)), style='italic',
                bbox={'facecolor': 'gray', 'alpha': 0.3, 'pad': 10}, transform=axs[2].transAxes,
                horizontalalignment='right', verticalalignment='baseline')

    axs[3].plot(np.linspace(2*const.pi*values['frequ'][0], 2*const.pi*values['frequ'][len(values['frequ']) - 1], 2), np.zeros(2), color='darkblue', marker=None, ls='solid', linewidth=3)
    axs[3].plot(frequ_res_phi, res_corr_phi, marker='.', linewidth=0, color='darkred', label='Residuen')

    axs[0].set_title(r'Resonanzkurve $\omega_{0} = {1} \pm {2}$ [1/s]'.format(i, round(popt[1], 2), round(np.sqrt(pcov[1, 1]), 2)), size=20)
    axs[0].set_ylabel(r'$\zeta_{0}$ normiert'.format(i, 'max'), size=15)

    axs[1].set_ylabel(r'Residuals', size=15)

    axs[2].set_ylabel(r'Phase $\phi$', size=15)

    axs[3].set_ylabel(r'Residuals', size=15)
    axs[3].set_xlabel(r'Frequenz $\omega$ [$\frac{1}{s}$]', size=15)

    for i in range(4):
        axs[i].grid(True)
        axs[i].set_xlim(xlim0, xlim1)
        #a.legend(frameon=True, loc='best')
        if i % 2 != 0:
            axs[i].yaxis.set_label_position('right')
            axs[i].yaxis.tick_right()
    plt.savefig(filepath, format='PDF')
    #plt.show()

    ############
    #Wiedergabe#
    ############
    return unp.uarray([popt[0], popt[1], popt[2], chi_ndf, p, chi_ndf_phi, p_phi, popt_phi[2], popt_phi[1]], np.sqrt([pcov[0, 0], pcov[1, 1], pcov[2, 2], 0, 0, 0, 0, pcov_phi[2,2], pcov_phi[1,1]]))


def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))


plt.style.use('classic')

############
#Konstanten#
############
d = 200 * const.micro
l = 2*const.centi
E = 98 * const.giga
rho = 85*1e+3
alpha = np.array([1.424987, 0.992249, 1.000198])
omega_res_lit = []
for i in range(3):
    omega_res_lit.append(2 * const.pi * alpha[i]*(2*i+1)**2*const.pi / (16*np.sqrt(3)) * d / l**2 * np.sqrt(E/rho))

###########################
#Auswertung der Frequenzen#
###########################

data = read_data('data/Grundfrequenz.txt', 0)
data1 = read_data('data/Resonanzkurve1.txt', 0)
data2 = read_data('data/Resonanzkurve2.txt', 0)
data3 = read_data('data/Teil2Messung.txt', 0)


values2 = plot_data(data2, 410, 480, 30000, 31500, 'results/Resonanzfrequenz2.pdf', p0=[45, 30734, 193], phi_start=406, phi_stop=470, i=2, pphi=[-5.24e+2, 4e+3, 30739, const.pi / 2])
values0 = plot_data(data, 0, len(data['frequ']), 1600, 2200, 'results/Grundfrequenz.pdf', p0=[10, 1931, 77], phi_start=110, phi_stop=140, i=0, pphi=[-9e+2, 2.22e+4, 1.929e+3, 1.2016])
values1 = plot_data(data1, 450, 490, 10750, 11150, 'results/Resonanzfrequenz1.pdf', p0=[36, 10940, 154], phi_start=460, phi_stop=490, i=1, pphi=[-4.6e+2, 6.48e+3, 10939, const.pi / 2])

########################
#Temperaturabhängigkeit#
########################

temperatures = []
dt = []
amp = []
delta_amp = []
frequ = []
popt_t = []
pcov_t = []
chi_ndf = []
omega_res = []
#Auslese der Daten, da alle in einer Datei gespeichert#
for i in range(1, 11):
    temperatures.append(data3['Amp'][i * 121 - 1])
    dt.append(data3['delta_Amp'][i * 121 - 1])
    f = []
    a = []
    da = []
    for j in range(0, 120):
        if data3['Amp'][(i - 1) * 121 + j] < 6e-5:
            a.append(data3['Amp'][(i - 1) * 121 + j])
            da.append(data3['delta_Amp'][(i - 1) * 121 + j])
            f.append(2 * const.pi * data3['frequ'][(i - 1) * 121 + j])
    amp.append(a)
    frequ.append(f)
    delta_amp.append(da)
#Plotten und Extrapolieren#
for i in range(len(temperatures)):
    popt, pcov = curve_fit(lorentzian, frequ[i][75:100], amp[i][75:100], sigma=delta_amp[i][75:100])
    chi, p = chisquareValue(frequ[i][75:100], amp[i][75:100], delta_amp[i][75:100], popt, lorentzian)

    plt.clf()
    plt.errorbar(frequ[i], amp[i], yerr=delta_amp[i], marker='.', linewidth=0, color='darkred', label='Messwerte')
    plt.errorbar(frequ[i][75:100], amp[i][75:100], yerr=delta_amp[i][75:100], marker='.', linewidth=0, color='darkblue', label='Fitwerte')
    plt.plot(np.linspace(1400, 2200, 100), lorentzian(np.linspace(1400, 2200, 100), *popt), marker=None, ls='--', color='darkblue', label='Fitkurve')
    plt.vlines(popt[1], 0, np.amax(lorentzian(np.linspace(1400, 2200, 100), *popt)), color='gray', label=r'$\omega_0$ = {0} [1/s]'.format(round(popt[1], 2)))
    plt.text(1600, 0.000045, r'$\chi$ / ndf = {0}'.format(round(chi, 2)), style='italic', bbox={'facecolor': 'gray', 'alpha' : 0.3, 'pad' : 10})

    plt.tight_layout()
    plt.xlim(1400, 2200)
    plt.title(r'Resonanzkurve bei T = {0} °C'.format(round(temperatures[i], 2)), size=20)
    plt.ylabel(r'Amplitude', size=15)
    plt.xlabel(r'Frequenz $\omega$ [1/s]', size=15)
    plt.grid(True)
    plt.legend(frameon=True, loc='best')
    plt.savefig('results/temp_results/TempPlot{0}.pdf'.format(i), format='PDF')
    #plt.show()

    w = unc.ufloat(popt[1], np.sqrt(pcov[1, 1]))
    g = unc.ufloat(popt[2], np.sqrt(pcov[2, 2]))
    Q = w / g
    w_R = w * sqrt(1-0.327/Q**2)
    omega_res.append(w_R)

#############################
#Als Funktion der Temperatur#
#############################
popt, pcov = curve_fit(linear_fit, temperatures[2:5], unp.nominal_values(omega_res[2:5]), sigma=unp.std_devs(omega_res[2:5]))
plt.clf()
plt.style.use('classic')
plt.errorbar(temperatures, unp.nominal_values(omega_res), xerr = dt, yerr=unp.std_devs(omega_res), marker='.', color='darkred', linewidth=0, label='Messwerte')
plt.errorbar(temperatures[2:5], unp.nominal_values(omega_res[2:5]), xerr = dt[2:5], yerr=unp.std_devs(omega_res[2:5]), marker='.', color='darkblue', linewidth=0, label='Fitwerte')
plt.plot(np.linspace(40, 70, 2), linear_fit(np.linspace(40, 70, 2), *popt), color='darkblue', ls='--', marker=None, label='Ausgleichsgerade')
plt.text(45, 1800, r'f(x) = {0} x + {1}'.format(round(popt[0], 2), round(popt[1], 2)), style='italic', bbox={'facecolor': 'gray', 'alpha': 0.3, 'pad': 15})

plt.xlabel(r'Temperatur T[°C]', size=15)
plt.ylabel(r'Resonanzfrequenz $\omega_R$ [1/s]', size=15)
plt.title(r'Resonanzfrequenz als Funktion der Temperatur', size=20)

plt.tight_layout()
plt.legend(frameon=True, loc='best')
plt.grid(True)
plt.savefig('results/temp_results/Ausgleich.pdf', format='PDF')
plt.show()

a = unc.ufloat(popt[0], np.sqrt(pcov[0,0]))
b = unc.ufloat(popt[1], np.sqrt(pcov[1,1]))
print(a)
print(b)

###################################################
#Methode mit der Grundfrequenz und dem Acos signal#
###################################################
y_value = [] #unp.uarray(values0['Amp'], values0['delta_Amp']) * cos(unp.uarray(values0['phi'], values0['delta_phi']) + const.pi)
y_value_err = []

for i in range(len(data['Amp'])):
    y_value.append(unc.nominal_value(unc.ufloat(data['Amp'][i], data['delta_Amp'][i])) * cos(unc.ufloat(data['phi'][i], data['delta_phi'][i]) + const.pi))
    y_value_err.append(unc.std_dev(unc.ufloat(data['Amp'][i], data['delta_Amp'][i])) * cos(unc.ufloat(data['phi'][i], data['delta_phi'][i]) + const.pi))

y_max = unc.ufloat(np.amax(unp.nominal_values(y_value[114:141])), np.amax(unp.std_devs(y_value_err[114:141])))
y_min = unc.ufloat(np.amin(unp.nominal_values(y_value[114:141])), np.amax(unp.std_devs(y_value_err[114:141])))
i_1 = np.where(unp.nominal_values(y_value) == unp.nominal_values(y_max))
i_2 = np.where(unp.nominal_values(y_value) == unp.nominal_values(y_min))


plt.clf()
plt.grid(True)
plt.title('Bestimmung der Güte mit der Grundresonanzkurve', size=15)
plt.ylabel('Amplitude', size=15)
plt.xlabel('Anregungsfrequenz [Hz]', size=15)
plt.xlim(1500, 2500)
plt.errorbar(2*const.pi*data['frequ'], unp.nominal_values(y_value), yerr=unp.std_devs(y_value_err), marker='.', linewidth=0, color='darkred', label=r'$Acos(\phi + \pi)$')
plt.errorbar(2*const.pi*data['frequ'], data['Amp'], yerr=data['delta_Amp'], marker='.', linewidth=0, color='darkblue', label='Amplitude')
plt.plot(np.linspace(1500, 2500, 2), np.zeros(2), marker=None, ls='--', color='black')
plt.vlines(unc.nominal_value(values0[1]), 0, np.amax(data['Amp'][114:141]), color='gray', label=r'$\omega_0$ = {0} Hz'.format(round(unc.nominal_value(values0[1]), 2)))
plt.vlines(2*const.pi*data['frequ'][i_1[0]], 0, unc.nominal_value(y_max), color='green', label=r'$\delta_1$ = {0} Hz'.format(round(2*const.pi*data['frequ'][int(i_1[0])], 2)))
plt.vlines(2*const.pi*data['frequ'][i_2[0]], 0, unc.nominal_value(y_min), color='green', label=r'$\delta_2$ = {0} Hz'.format(round(2*const.pi*data['frequ'][int(i_2[0])], 2)))
plt.legend(frameon=True, loc='best')
#plt.show()
plt.savefig('results/Methode2.pdf', format='PDF')

delta2 = 2*const.pi*data['frequ'][int(i_1[0])] - 2*const.pi*data['frequ'][int(i_2[0])]
Q_1 = values0[1] / delta2
omega_r = values0[1]*sqrt(1-0.327/Q_1**2)
print('omega0 '+str(values0[1]))
print('omegar '+str(omega_r))
print('Mit der zweiten Methode beträgt Q = {0}'.format(Q_1))
#######
#Mathe#
#######
results = np.array([values0, values1, values2])
c = np.array([-0.327, 0.108, 0.524])
A = []
omega_0 = []
omega_0_phi = []
omega_r = []
p_a = []
p_chi = []
Q = []
gamma = []
gamma_phi = []
for i in results:
    A.append(i[0])
    omega_0.append(i[1])
    omega_0_phi.append(i[7])
    gamma.append(i[2])
    gamma_phi.append(i[8])
    p_a.append(i[4])
    p_chi.append(i[6])

for i in range(len(results)):
    Q.append(omega_0[i] / gamma[i])
    omega_r.append(omega_0[i] * sqrt(1+c[i] / Q[i]**2))


###########
#Vergleich#
###########
verh_1 = omega_0[1] / omega_0[0]
verh_2 = omega_0[2] / omega_0[0]

verh_lit_1 = 6.267
verh_lit_2 = 17.548

sigma_1 = calc_sigma(verh_1, verh_lit_1)
sigma_2 = calc_sigma(verh_2, verh_lit_2)

v1 = np.array([verh_1, verh_lit_1, sigma_1])
v2 = np.array([verh_2, verh_lit_2, sigma_2])

q = np.array([Q_1, Q[0], calc_sigma(Q_1, Q[0])])
##Feedback
print('\n In Aufsteigender Reihenfolge')
print('Omega_0 beträgt\n' + str(omega_0))
print('Omega_0 Phi beträgt\n' + str(omega_0_phi))
print('Gamma beträgt\n' + str(gamma))
print('Gamma Phi beträgt\n' + str(gamma_phi))
print('Güte beträgt\n' + str(Q))
print('Omega_R \n' + str(omega_r))
print('p der Amplitude beträgt\n' + str(p_a))
print('p der Phase beträgt\n' + str(p_chi))
print('Erstes Verhältnis '+ str(v1))
print('Zweite Verhältnis '+ str(v2))
print('Verleich der beiden Methoden liefert\n ' + str(q))
