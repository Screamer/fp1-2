\documentclass{beamer}
\usepackage{eulervm}

\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{svg} %.svg Datein
\usepackage{color,soul} % Unterstreichen
\usepackage{animate}
\usepackage{graphicx}
\usepackage{tikz}

\usetheme{CambridgeUs}
\usecolortheme{rose}

%Arg Min Definition
\DeclareMathOperator*{\argminA}{arg\,min}

\title[S01/02/03/04]{Statistische Methoden in der Experimentalphysik}
%\subtitle{Platzhalter 2}
\author[Behn, Bartels]{Niklas Behn \and Jan Bartels}
\institute{}
\date{Ruprecht-Karls-Universität Heidelberg, 2.11.22}
%Logo
\titlegraphic{
    \includegraphics[width=2cm]{svg/reines_logo}
}


\begin{document}
\frame{\titlepage}

\begin{frame}{Übersicht}
    \only<1>{
    \begin{itemize}
        \item Durchführung erfolgt von zu Hause
        \item Aufgaben werden in Form von Jupyter-Notebooks gestellt
        \item 4 Themenbereche unterteilt \begin{enumerate}
            \item \textbf{Fehlerfortpflanzung}
            \item \textbf{Methode der kleinsten Quadrate}
            \item \textbf{Maximum-Likelihood-Methode}
            \item \textbf{Bayessche Parameterschätzung}
        \end{enumerate}
       \end{itemize}
    }
    \only<2->{
        \begin{enumerate}
            \item \textbf{Fehlerfortpflanzung} \begin{itemize}
                \item<2->Fehlerfortpflanzung
            \end{itemize}
            \item \textbf{Methode der kleinsten Quadrate} \begin{itemize}
                \item<2-> Linearer $\chi^2$-Fit
                \item\textcolor<3->{gray}{Simultaner $\chi^2$-Fit}
            \end{itemize}
            \item \textbf{Maximum-Likelihood-Methode} \begin{itemize}
                \item\textcolor<3->{gray}{Mittlere Lebensdauer beim exponentiellen Zerfall}
                \item Ungebinnter Maximum-Likelihood-Fit
            \end{itemize}
            \item \textbf{Bayessche Parameterschätzung}\begin{itemize}
                \item Bayessche Parameterschätzung
            \end{itemize}
        \end{enumerate}
    }
\end{frame}
\section{Einleitung}
\begin{frame}{Wiederholung}
\begin{block}{Wahrscheinlichkeitsdichte}
    Die Wahscheinlichkeit $P$ einen Wert $x$ im Intervall $\left[x,x+dx\right]$ zu finden ist gegeben durch die 
    \textbf{Wahrscheinlichkeitsdichtefunktion} $f(x)$:
    \begin{equation}
        P(x\in\left[x,x+dx\right]) = f(x)\cdot dx
    \end{equation}
    diese folgt der Normierungsbedingung $\int_\Omega f(x)dx=1$.\\[6pt]
    Die Wahrscheinlichkeit einen im Intervall $\left[a,b\right]$ befindlichen Wert zu erhalten berechnet sich durch:
    \begin{equation}
        P(a\leq x \leq b) = \int_a^b f(\sigma)d\sigma = F(b)-F(a)
    \end{equation}\vspace{6pt}
\end{block}
\end{frame}
\begin{frame}{Wiederholung}
\begin{block}{Erwartungswert und Zentrale Momente}
    \only<1>{
    Der Erwartungswert $E(x)$ einer Wahrscheinlichkeitsdichte $f(x)$
    \begin{equation}
        E(x) = \mu = \int_{-\infty}^{\infty}x\cdot f(x)dx
    \end{equation}
    \textbf{Zentrale l-te Momente} sind der Erwartungswert der Funktion
    \begin{equation}
        h_l(x)=(x-c)^l
    \end{equation}
    der Variable $x$ und des Punktes $c$.
    \begin{equation}
        \alpha_i = \int_{-\infty}^{\infty}(x-\mu)^i\cdot f(x)dx
    \end{equation}
    }
    \only<2>{
        Die relevantesten Momente sind
        \begin{enumerate}
            \item $\alpha_0 = \int_\mathbb{R}f(x)dx=1$ Normierung
            \item $\alpha_1 = \int_\mathbb{R}(x-\mu)f(x)dx=0$ Erwartungswert
            \item $\alpha_2 = \int_\mathbb{R}(x-\mu)^2f(x)dx=\sigma^2(x)$ Varianz
        \end{enumerate}
        Höhere Momente beschreiben die Ausbreitung der Verteilung.
    }
\end{block}
\end{frame}
\begin{frame}{Wiederholung}
\begin{block}{Kovarianz}
Die \textbf{Kovarianz} $\rho_{(x,y)}$ ist ein Maß für die gemeinsame Variabilität
\begin{equation}
    cov(x,y) = \int_\mathbb{R}\int_\mathbb{R}(x-\mu_x)(y-\mu_y)f(x,y)dx\cdot dy
\end{equation}
Korrelationskoeffizient $\rho_{(x,y)}$
\begin{equation}
    \rho_{(x,y)} = \frac{cov(x,y)}{\sigma_x\cdot\sigma_y}
\end{equation}
Die Variablen $x$ und $y$ gelten als unabhängig wenn folgendes gilt
\begin{equation}
    f(x,y) = g(x)\cdot h(x)
\end{equation}
Gilt dies, so ist $cov(x,y)=0$, umgekehrt jedoch nicht.
\end{block}
\end{frame}

\section{Fehlerfortpflanzung}
\begin{frame}[t]{Fehlerfortpflanzung}
    \vspace{-10pt}
    \only<1>{
    \begin{block}{Kovarianzmatrix, Fehlermatrix}
        Die \textbf{Kovarianzmatrix} C der Varianz von Messgrößen $x_i$ ist gegeben durch
        \begin{equation}
            C = 
            \begin{pmatrix}
                \sigma_{x_1}^2  &           &   cov(x_i,x_j)    \\
                                &   \ddots  &                   \\
                cov(x_j,x_i)    &           &   \sigma_{x_n}^2
            \end{pmatrix}
        \end{equation}
        Für einen Satz von $m$ Funktionen $(f_1,\cdots, f_m)$ der Variablen $(x_1,\cdots,x_n)$ berechnet man die \textbf{Fehlermatrix}
        $E_{kl}$
        \begin{equation}
            E_{kl} = \sum_{i,j=1}^{n,n}\frac{\delta f_k}{\delta x_i}\frac{\delta f_l}{\delta x_j}\rho_{ij}\sigma_{x_i}\sigma_{x_j}
        \end{equation}
        Die Diagonale entspricht den Unsicherheiten der Funktionen $\sigma^2(f(x_1,\cdots, x_n))$
    \end{block}
    }
    \only<2,3>{
        1.1/1.2 Bestimme die Unsicherheiten für folgende korrelierten Ausdrücke
        \begin{columns}
            \begin{column}{0.2\textwidth}
                \begin{equation*}
                    z_1 = x+y
                \end{equation*}
            \end{column}
            \begin{column}{0.2\textwidth}
                \begin{equation*}
                    z_2 = x-y
                \end{equation*}
            \end{column}
            \begin{column}{0.2\textwidth}
                \begin{equation*}
                    z_3 = x\cdot y
                \end{equation*}
            \end{column}
            \begin{column}{0.2\textwidth}
                \begin{equation*}
                    z_4 = x/y
                \end{equation*}
            \end{column}
            \begin{column}{0.2\textwidth}
                \begin{equation*}
                    z_4 = (xy)^n
                \end{equation*}
            \end{column}
        \end{columns}
        \only<3>
        {
        \begin{exampleblock}{Durchführung}
            Verwende die \textbf{Fehlermatrix}
            \begin{equation*}
                E_{11} = \sum_{i,j=1}^{2,2}\frac{\delta f}{\delta x_i}\frac{\delta f}{\delta x_j}\rho_{ij}\sigma_{x_i}\sigma_{x_j} = (\mathbf{\nabla}\cdot f)^T\cdot C\cdot \mathbf{\nabla} f
            \end{equation*}
            Schreibe explizit aus
            \begin{equation*}
                \sigma^2(z)=\left(\frac{\delta f}{\delta x}\right)^2\sigma_x^2+(x\leftrightarrow y) + 2\left(\frac{\delta f}{\delta x}\right)\left(\frac{\delta f}{\delta y}\right)\rho\sigma_x\sigma_y
            \end{equation*}
        \end{exampleblock}
        }
    }
    \only<4->{
        \begin{enumerate}
        \item $z_1 = x+y $\begin{itemize}
            \item $\sigma(z_1) = \sqrt(\sigma_x^2+\sigma_y^2+2\rho\sigma_x\sigma_y)$
        \end{itemize}
        \item $z_2=x-y   $\begin{itemize}
            \item $\sigma(z_2) = \sqrt(\sigma_x^2+\sigma_y^2-2\rho\sigma_x\sigma_y)$
        \end{itemize}
        \item $z_3 = xy  $\begin{itemize}
            \item $\sigma(z_3) = \sqrt(\sigma_x^2+\sigma_y^2+2\rho\sigma_x\sigma_y)\cdot z_3$
        \end{itemize}
        \item $z_4 = x/y $\begin{itemize}
            \item $\sigma(z_4) = \sqrt(\frac{\sigma_x^2}{x^2}+\frac{\sigma_y^2}{y^2}-2\frac{\rho\sigma_x\sigma_y}{xy})\cdot z_4$
        \end{itemize}
        \item $z_5=(xy)^n$\begin{itemize}
            \item $\sigma(z_5) = \sqrt(\frac{n^2(\sigma_x^2y^2+\sigma_y^2x^2+2\rho\sigma_x\sigma_yxy)}{xy})\cdot z_5$ 
        \end{itemize}
        \end{enumerate}
    }
\end{frame}
\begin{frame}[t]{Fehlerfortpflanzung}
    \only<1>{
    1.3 Für die Beschleunigung eines einfachen Pendels mit $g=4\pi^2\frac{L}{T^2}$\\[20pt]
    \begin{columns}
        \begin{column}{0.3\textwidth}
            \centering $g=4\pi^2\frac{L}{T^2}$
        \end{column}
        \begin{column}{0.7\textwidth}
            \centering $\sigma_g = \frac{4\pi^2\sqrt{4L^2\sigma_T^2-4LT\rho\sigma_L\sigma_T+T^2\sigma_L^2}}{T^3}$
        \end{column}
    \end{columns}\vspace{20pt}
    2. Bestimme das Volumen eines Zylinders mit $r=2cm$ und $h=3cm$ sowie einem Fehler von $\sigma_r = \sigma_h = 0.5cm$\\[20pt]
    \begin{columns}
        \begin{column}{0.3\textwidth}
            \centering $V=\pi hr^2 = 37.7cm^3$
        \end{column}
        \begin{column}{0.7\textwidth}
            \centering $\sigma_V = \pi r\sqrt(4h^2\sigma_r^2+4hr\rho\sigma_h\sigma_r+r^2\sigma_h^2) = 2.5cm^3$
        \end{column}
    \end{columns}\vspace{20pt}
    3. Schreibe die Kovarianzmatrix V der Unsicherheiten von $(x,y)$ als Kovarianzmatrix U der Koordinaten $r=\sqrt{x^2+y^2}$ und $\theta=atan2(y,x)$ um.
    }
    \only<2->{
    3. Schreibe die Kovarianzmatrix V der Unsicherheiten von $(x,y)$ als Kovarianzmatrix U der Koordinaten $r=\sqrt{x^2+y^2}$ und $\theta=atan2(y,x)$ um.
        \only<3>{
        \begin{exampleblock}{Koordinatenwechsel}
            Schreibe $f_1(x_1,x_2) = \sqrt{x_1^2+x_2^2}$ und $f_2(x_1,x_2) = atan2(x_1,x_2)$ mit Variablen $(x_1,x_2)$, nutze erneut \textbf{Fehlermatrix}
            \begin{equation*}
                \mathbf{E = G\cdot V \cdot G^T}
            \end{equation*}
            wobei
            \begin{equation*}
                \mathbf{G_{ij}} = \frac{\delta f_i}{x_j}
            \end{equation*}
            die \textbf{Transformationsmatrix} / \textbf{Jacobi-Matrix} darstellt.
        \end{exampleblock}
        }
        \only<4->{
            \\[20pt]Erhalte\\[20pt]
            \begin{columns}
                \begin{column}{0.5\textwidth}
                    \begin{equation*}
                        \sigma_r = \frac{\sqrt(\sigma_x^2x^2+\sigma_y^2y^2)}{\sqrt(x^2+y^2)}
                    \end{equation*}
                \end{column}
                \begin{column}{0.5\textwidth}
                    \begin{equation*}
                        \sigma_{\theta} = \frac{\sqrt{\sigma_x^2y^2+\sigma_y^2x^2}}{x^2+y^2}
                    \end{equation*}
                \end{column}
            \end{columns}
        }
    }
\end{frame}
\section{Methode der kleinsten Quadrate}
\begin{frame}{Methode der kleinsten Quadrate}
    \only<1>{
    \begin{block}{Chi-Verteilung}
        Die $\mathbf{\chi^2}$\textbf{-Verteilung} eines Datensatzes aus n normalverteilten Messwerten $y_i$ an den Orten $x_i$
        mit Standardabweichungen $\sigma_i$ und eine Modellvorhersage $f(x;\vec{\lambda})$ mit vorher festgelegten Parametern $\vec{\lambda}$ resultiert aus
        \begin{equation}
            \chi^2 = \sum_{i=1}^{n}\frac{(y_i-f(x_i;\vec{\lambda}))^2}{\sigma_i^2}
        \end{equation}
        Die Anzahl an Freiheitsgraden beträgt die Anzahl an Messwerten abzüglich der Anzahl an Parametern.\\
        \begin{equation}
            n.d.f = (\text{Messwerte})-(\text{Parameter})
        \end{equation}
    \end{block}
    }
    \only<2>{
        \begin{block}{p-Wert}
            Der \textbf{p-Wert} $P(\chi^2)$ ist die Wahrscheinlichkeit, einen größeren $\chi^2$-Wert zu erhalten, unter der Annahme, dass das verwendete Modell korrekt ist.
            \begin{equation}
                P(\chi^2) = 1 - F(\chi^2)
            \end{equation}
            mit
            \begin{equation}
                F(\chi^2) = \int_{0}^{\chi^2}\frac{z^{n/2-1}e^{-z/2}}{2^{n/2}\Gamma\left(\frac{n}{2}\right)}dz
            \end{equation}
            \vspace{10pt}
        \end{block}
    }
    \only<3>{
        \begin{block}{reduziertes $\chi^2$}
            Zur Beschreibung der Güte wird in Publikationen das \textbf{reduzierte} $\chi_{n.d.f}^2$ benutzt
            \begin{equation}
                \chi_{n.d.f}^2 = \frac{\chi^2}{n.d.f}
            \end{equation}
            Die Unsicherheit skaliert mit $1/\sqrt{n}$.\\
            Bei $\mathbf{\chi_{n.d.f}^2 >>1}$ ist P sehr klein, d.h. die Fehler der Einzelmessungen wurden unterschätzt oder das Experiment wird durch die Funktion $f(x_i;\vec{\lambda})$ nicht vollständig beschrieben.\\
            Bei $\mathbf{\chi_{n.d.f}^2 <<1}$ ist P sehr groß, d.h. die Übereinstimmung ist zu gut. Entweder sind die Fehler zu großzügig oder statistische Fluktuationen sind enthalten.
        \end{block}
    }
    \only<4>{
        \begin{block}{Methode der kleinsten Quadrate}
            Für $n$ gegebene Datenpunkte $(x_i,y_i\pm \sigma_i)$ wird die Funktion $f(x;\vec{\lambda})$ bestimmt, indem $\chi^2$ \textbf{minimiert} wird.
        \begin{equation}
            \argminA_{\vec{\lambda}}\left(\chi^2\right) = \argminA_{\vec{\lambda}}\left(\sum_{i=1}^n\frac{(y_i-f(x_i;\vec{\lambda}))^2}{\sigma_i^2}\right)\quad\rightarrow\quad\frac{d\chi^2}{d\lambda_i}=0
        \end{equation}
        \begin{figure}
            \centering
            \includegraphics[width=0.25\textwidth]{svg/linear_fit.pdf}
            \label{fig:1}
        \end{figure}
        \vspace{10pt}
        \end{block}
    }
\end{frame}
\begin{frame}[t]{Methode der kleinsten Quadrate}
    1. Gegeben sei $y(x)=\theta_0+\theta_1x+\theta_2x^2$, sowie ein Datensatz $(x_i,y_i\pm\sigma_i)$. Bestimme die optimalen Parameter $\theta_i$ anhand vorheriger Methoden.
    \only<1,2>{
    \begin{exampleblock}{Durchführung}
        \only<1>{
        Die $\chi^2$-Funktion der Werte $\mu_i=y(x_i;\vec{\theta})$ und der Covarianzmatrix $\mathbf{C}$ lautet
        \begin{equation*}
            \chi^2 = (\vec{y}-\vec{\mu})^T\cdot\mathbf{C}^{-1}\cdot(\vec{y}-\vec{\mu})
        \end{equation*}
        Umschreiben von $y(x)$ ergibt
        \begin{equation*}
            \vec{\mu} = \mathbf{A}\cdot\vec{\theta} \quad \text{mit}\quad A_{ij} = x_i^{j-1} \quad \mathbf{A}\in\text{Mat(7,3,}\mathbb{R}\text{)}
        \end{equation*}
        Einsetzen
        \begin{equation*}
            \mathbb{\nabla}_\theta\chi^2=-2\mathbf{A}^T\cdot\mathbf{C}^{-1}\cdot (\vec{y}-\mathbf{A}\cdot\vec{\theta})=0
        \end{equation*}
        }
        \only<2>{
        Umformen
        \begin{equation*}
            \vec{\theta} = \left(\mathbf{A}^T\mathbf{C}^{-1}\mathbf{A}\right)^{-1}\mathbf{A}^T\mathbf{C}^{-1}\vec{y}\coloneqq\mathbf{G}\vec{y}
        \end{equation*}
        Sowie Kovarianzmatrix $\mathbf{C}_\theta$ anhand der Transformation der Kovarianzmatrix der Messwerte
        \begin{equation*}
            \mathbf{C}_\theta = \mathbf{G}\cdot\mathbf{C}\cdot\mathbf{G}^T
        \end{equation*}
        }
    \end{exampleblock}
    }
    \only<3>{
        \vspace{20pt}
        \begin{equation*}
            \vec{\theta}=
            \begin{pmatrix}
                \theta_0    \\ \theta_1 \\  \theta_2
            \end{pmatrix}
            =
            \begin{pmatrix}
                5   \\  1.3 \\  -4.9
            \end{pmatrix}
        \end{equation*}
        \begin{equation*}
            \mathbf{C}_\theta = 
            \begin{pmatrix}
                0.1     &   0       &   -0.27   \\
                0       &   0.48    &   0       \\
                -0.27   &   0       &   2       \\
            \end{pmatrix}
        \end{equation*}
        \vspace{10pt}
    }
\end{frame}
\begin{frame}[t]{Methode der kleinsten Quadrate}
    \only<1>{
        \begin{figure}
            \centering
            \includegraphics[width=0.8\textwidth]{svg/chi.pdf}
            \caption{$\chi^2$-Verteilung. In rot der $\chi^2$-Wert, sodass die $P(\chi^2)=32\%$ beträgt.}
            \label{fig:2}
        \end{figure}
    }
    \only<2>{
        \begin{figure}
            \centering
            \includegraphics[width=0.6\textwidth]{svg/fit.pdf}
            \caption{Anhand der Methode der kleinsten Quadrate bestimme Parabel. In hellgrün ist der 1$\sigma$-Bereich gekennzeichnet}
            \label{fig:3}
        \end{figure}
    }
    \only<3->{
        4. Vergleiche dies mit dem Ergebnis, welches \textbf{iminuit} liefert.
        \begin{itemize}
            \item<4-> iminuit minimalisiert numerisch eine Funktion $\chi^2$ gegebener Methode sowie Parameter $\theta_i$
            \item<5-> Ergebnisse stimmen überein
        \end{itemize}
    }
    \vspace{10pt}
    \only<6->{
        5. Beschreibt eine Gerade die Messwerte?
        \only<7->{
            \begin{equation*}
                P(\chi^2) < 0.5\%
            \end{equation*}
            \centering\ul{Folglich beschreibt das Modell nicht die Physik.}
        }
    }
\end{frame}
\section{Maximum Likelihood Methode}
\begin{frame}[t]{Maximum Likelihood Methode}
    \only<1>{
        \begin{block}{Likelihood-Funktion}
            Für allgemeine Wahrscheinlichkeitsdichtefunktionen $f(x;\vec{\lambda})$ der Messwerte $(x_1,\cdots,x_N)$ benutzt man die \textbf{Likelihood-Funktion} $L$
            \begin{equation}
                L(x;\vec{\lambda}) = \prod_{i=1}^{N}f(x_i,\vec{\lambda})
            \end{equation}
            Die beste Schätzung $\vec{\lambda}_{ML}$ maximiert diese
            \begin{equation}
                \vec{\lambda}_{ML} = \argminA_{\vec{\lambda}} L(x;\vec{\lambda})\quad\rightarrow\quad\frac{\delta}{\delta\lambda_i}L(x;\lambda_i)=0\quad i\in\left[1,N\right]
            \end{equation}
        \end{block}
    }
    \only<2>{
        \begin{block}{Log-Likelihood-Funktion}
            Numerisch vorteilhafter ist die \textbf{Log-Likelihood-Funktion} $\mathcal{L}(x;\vec{\lambda})$
            \begin{equation}
                \mathcal{L}(x;\vec{\lambda}) = ln(L(x;\vec{\lambda})) = \sum_i^N ln(f(x_i;\vec{\lambda}))
            \end{equation}
            Entwickelt man diese um ihr Maximum ergibt sich die Standardabweichung
            \begin{equation}
                \sigma_{\lambda_j}^2 = \left(-\frac{\delta^2\mathcal{L}}{\delta_j\delta_j}\Big|_{\lambda=\lambda_{ML}}\right)^{-1}
            \end{equation}
            Eine alternative Abschätzung liefert im Sonderfall eines Parameters
            \begin{equation}
                ln(L(\lambda_{ML}\pm\sigma_\lambda)) = ln(L_{max})-\frac{1}{2}
            \end{equation}
        \end{block}
    }
    \only<3->{
        \only<3>{
            Die Wahrscheinlichkeitsdichtefunktion $f(x;a)$ für den Winkel $\alpha$ sei gegeben durch
            \begin{equation*}
                f(x;a) = \frac{1+ax}{2},\quad x=\cos (\alpha),\quad a\in\left[-1,-1\right]
            \end{equation*}
            1. Bestimme den Schätzwert $\hat{a}$ und dessen Unsicherheit mittels der Maximum Likelihood Methode. Nutze hierfür ein numerisches Programm zur Minimalisierung, e.g. \textbf{iminuit}.    
        }
        \only<4>{
            \begin{exampleblock}{Durchführung}
                \begin{equation*}
                    \mathcal{L}(x;a) = \sum_{i=1}^N \ln\left(\frac{1+ax_i}{2}\right)=\sum_{i=1}^N\ln(1+ax_i)-N\ln(2)
                \end{equation*}
                \begin{equation*}
                    \frac{\delta\mathcal{L}}{\delta a} = \sum_{i=1}^N\frac{x_i}{1+ax_i}\overset{!}{=}0\qquad\text{(löse numerisch)}
                \end{equation*}
                \begin{equation*}
                    \sigma_{\hat{a}}^2=-\left(\frac{\delta^2\mathcal{L}}{\delta a^2}\Big|_{a=\hat{a}}\right)^{-1} = \left(\sum_{i=1}^N\frac{x_i^2}{(1+\hat{a}x_i)^2}\right)^{-1}
                \end{equation*}
                \begin{equation*}
                    g(x) = f(x_n)+f^{(1)}(x_n)\cdot (x-x_n)\quad\text{und}\quad g(x_{n+1})=0
                \end{equation*}
                \begin{equation*}
                    \Rightarrow f(x_n)+f^{(1)}(x_n)\cdot (x_{n+1}-x_n)=0\Leftrightarrow x_{n+1}=x_n+\frac{f(x_n)}{f^{(1)}(x_n)}
                \end{equation*}
            \end{exampleblock}
        }
        \only<5,6,7,8>{
            1. Bestimme den Schätzwert $\hat{a}$ und dessen Unsicherheit mittels der Maximum Likelihood Methode. Nutze hierfür ein numerisches Programm zur Minimalisierung, e.g. \textbf{iminuit}.
            \only<4>{
                \begin{figure}
                    \centering
                    \includegraphics[width=0.6\textwidth]{svg/ml_1.png}
                    \caption{Beispiel des angewandten Verfahrens nach Newton-Raphson}
                \end{figure}
            }
            \only<6,7,8>{
                \begin{columns}
                    \begin{column}{0.25\textwidth}
                        \begin{itemize}
                            \item<6-> 2 Interationen
                            \item<7-> $\hat{a}=0.100$
                            \item<8-> $\sigma_{\hat{a}} = 0.009$
                        \end{itemize}
                    \end{column}
                    \begin{column}{0.75\textwidth}
                        \begin{figure}
                            \centering
                            \includegraphics[width=0.6\textwidth]{svg/ml_2.pdf}
                            \caption{Log-Likelihood-Funktion $L(x;a)$}
                        \end{figure}
                    \end{column}
                \end{columns}
            }
        }
        \only<9>{
            2. Plotte das Histogramm der Daten sowie die Fit-Funktion.\\[20pt]
            \begin{minipage}[b]{0.45\linewidth}
                \begin{figure}
                    \centering
                    \includegraphics[width=\textwidth]{svg/ml_3.pdf}
                    \caption{Wahrscheinlichkeitsdichtefunktion}
                    \label{fig:a}
                \end{figure}
            \end{minipage}
            \hspace{0.5cm}
            \begin{minipage}[b]{0.45\linewidth}
                \begin{figure}
                    \centering
                    \includegraphics[width=\textwidth]{svg/ml_4.pdf}
                    \caption{Histogramm der Messwerte}
                    \label{fig:b}
                \end{figure}
            \end{minipage}
        }
        \only<10->{
            3. Bestimme $\pm\sigma_a$ anhand $\ln (L(\hat{a}\pm\sigma_a)) = \ln (L_{max})-\frac{1}{2}$. Ab welcher Anzahl an Messwerten $N$ ist die Differenz von $\sigma_a^+$ und $\sigma_a^-$ größer als 5\%?\\[10pt]
            \only<11,12>{
                Einsetzen liefert
                \begin{equation*}
                    \prod_{i=1}^{N}\left(1\pm\frac{\sigma}{1+\hat{a}x_i}\right)=e^{-1/2}
                \end{equation*}
            }
            \only<12>{
                \begin{center}
                    \resizebox{0.5\textwidth}{!}{
                        \begin{tabular}{|c|c|c|}
                            \hline
                            N   &   $\sigma_{\hat{a}}^+$    &   $\sigma_{\hat{a}}^-$\\
                            \hline
                            300 &   $0.76\% $ &   $0.75\%$\\
                            299 &   $5.34\% $ &   $5.65\%$\\
                            295 &   $16.19\%$ &   $19.32\%$\\
                            150 &   $46.97\%$ &   $88.56\%$\\
                            \hline
                        \end{tabular}
                    }
                \end{center}
            }
            \only<13>{
                \begin{minipage}[b]{0.45\linewidth}
                    \begin{figure}
                        \centering
                        \includegraphics[width=\textwidth]{svg/ml_5.pdf}
                        \caption{Nullstellenverfahren für N=300}
                        \label{fig:c}
                    \end{figure}
                \end{minipage}
                \hspace{0.5cm}
                \begin{minipage}[b]{0.45\linewidth}
                    \begin{figure}
                        \centering
                        \includegraphics[width=\textwidth]{svg/ml_6.pdf}
                        \caption{Nullstellenverfahren für N=150}
                        \label{fig:d}
                    \end{figure}
                \end{minipage}
            }
        }
    }
\end{frame}
\section{Bayessche Paramterschätzung}
\begin{frame}[t]{Bayessche Parameterschätzung}
    Ein Partikel besitzt zwei unterschiedliche Zerfallskanäle: A und B. Das Verzweigungsverhältnis in A zu zerfallen sei $f_A$.\\[10pt]
    \only<1-5>{
        1. Erläutere, warum es sich hierbei um eine Binomialverteilung handelt bei $N$ beobachteten Zerfällen.
        \begin{enumerate}
            \item<2->Zerfälle finden unabhängig voneinander statt.
            \item<3->Endliche Anzahl von Ereignissen
        \end{enumerate}
        \vspace{10pt}
        \only<4->{
            \begin{equation*}
                P(k|N) = \binom{N}{k} \cdot f_A^k(1-f_A)^{N-k}
            \end{equation*} 
        }
    }
    \only<5-7>{
        2. Erkäre die Bayessche Parameterschätzung.
        \only<6>{
            \begin{block}{Posterior-Wahrscheinlichkeitsfunktion $P(\vec{\lambda}|x)$}
                Die Informationen für den zu schätzenden Parameter $\vec{\lambda}=(\lambda_1,\cdots,\lambda_n)$ ist in der
                \textbf{Posterior-Wahrscheinlichkeitsfunktion} $P(\vec{\lambda}|x)$ enthalten. Dies lässt sich unter der Verwendung des Bayesschen Theorems
                schreiben als
                \begin{equation}
                    P(\vec{\lambda}|x)=\frac{L(x|\vec{\lambda})\pi (\vec{\lambda})}{\int L(x|\vec{\lambda})\pi (\vec{\lambda})d\vec{\lambda}}
                \end{equation}
                $\pi(\vec{\lambda})$ beschreibt die \textbf{A-priori-Wahrscheinlichkeitsverteilung}
            \end{block}
        }
        \only<7>{
            \begin{exampleblock}{Durchführung}
                Angewand auf das konkrete Beispiel
                \begin{equation*}
                    L(x|\vec{\lambda})=f(k|f_A)= \binom{N}{k} \cdot f_A^k\cdot(1-f_A)^{N-k}
                \end{equation*}\\[2pt]
                \begin{equation*}
                    P(\vec{\lambda}|x)=P(f_A|\vec{k})
                \end{equation*}\\[3pt]
                \begin{equation*}
                    P(f_A|\vec{k}) \propto f(k|f_A)\pi (f_A)
                \end{equation*}
            \end{exampleblock}
        }
    }
    \only<8-9>{
        3. Nehme $\pi (f_A)=1$ an. Bestimme die Posterior-Wahrscheinlichkeitsfunktion für $f_A$ für $N=1$ und $k=1$.\\[10pt]
        \only<9->{
            \begin{equation*}
                f(k=1|N=1) = f_A
            \end{equation*}
            \begin{equation*}
                P(f_A|k=1) = \frac{f_A\cdot 1}{\int_\mathbb{R}xdx}=2f_A
            \end{equation*}
        }
    }
    \only<10->{
        4. Zeichne die neuen Posterior-Wahrscheinlichkeitsfunktionen nach jedem zusätzlichen Zerfall: 'A', 'B', 'B', 'A'.\\[10pt]
        \only<11-12>{
            \begin{enumerate}
                \item<11->Ersetze den A-Priori durch den vorherigen Posterior
                \item<12->Ersetze die Likelihood-Verteilung
            \end{enumerate}
        }
        \only<13->{
            \begin{exampleblock}{Trick: scipy.stats.beta}
                \only<13->{
                \begin{equation*}
                    \Gamma (z) = \int_{\mathbb{R}_\geq}t^{z-1}e^{-t}dt
                \end{equation*}
                }
                \only<14->{
                    \begin{equation*}
                        P(f_A|(k,N)) = \frac{f_A^k\cdot (1-f_A)^{N-k}}{\int_0^1f_A^k\cdot (1-f_A)^{N-k} df_A}=\frac{f_A^k\cdot (1-f_A)^{N-k}}{B(k+1,N-k+1)}
                    \end{equation*}
                }
                \only<15->{
                    \begin{equation*}
                        P(f_A|(k,N)) = \frac{f_A^k\cdot (1-f_A)^{N-k}\Gamma (N+2)}{\Gamma (k+1)\Gamma (N-k+1)}
                    \end{equation*}
                }
            \end{exampleblock}
        }
    }
\end{frame}
\begin{frame}[t]{Bayessche Parameterschätzung}
    \only<1,2>{
        4. Zeichne die neuen Posterior-Wahrscheinlichkeitsfunktionen nach jedem zusätzlichen Zerfall: 'A', 'B', 'B', 'A'.\\[10pt]
        \begin{exampleblock}{Trick: scipy.stats.beta}
            \only<1,2>{
                \begin{equation*}
                    P(f_A|(k,N)) = \frac{f_A^k\cdot (1-f_A)^{N-k}\Gamma (N+2)}{\Gamma (k+1)\Gamma (N-k+1)}
                \end{equation*}
            }
            \only<2->{
                \begin{equation*}
                    P(f_A|(k,N)) = \beta (f_A, k+1, N-k+1)
                \end{equation*}
                $\beta (f_A, k+1, N-k+1)$ bezeichnet die scipy.stats.beta Funktion.
            }
        \end{exampleblock}
    }
    \only<3>{
        4. Zeichne die neuen Posterior-Wahrscheinlichkeitsfunktionen nach jedem zusätzlichen Zerfall: 'A', 'B', 'B', 'A'.\\[10pt]
        \begin{figure}
            \centering
            \animategraphics[autoplay, loop, width=0.5\textwidth]{1}{ani-}{0}{4}
            \caption{Änderung des Posterior nach jeder Beobachtung}
        \end{figure}
    }
    \only<4>{
        5. Berechne und plotte den Posterior nach 100 Beobachtungen, wobei 63 A-Zerfälle detektiert wurden.\\[10pt]
        \begin{figure}
            \centering
            \includegraphics[width=0.5\textwidth]{svg/final_plot.pdf}
            \caption{Posterior anhand $N=100$ und $k=63$}
        \end{figure}
    }
\end{frame}
\end{document}