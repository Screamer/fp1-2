\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}NaI(TI) Szintillator (Inorganisch)}{1}{}%
\contentsline {subsection}{\numberline {1.1}Signal des Szintillator}{1}{}%
\contentsline {subsection}{\numberline {1.2}Aufnahme eines Pulsspektrums}{1}{}%
\contentsline {subsection}{\numberline {1.3}Pulsrate als Funktion der Spannung}{3}{}%
\contentsline {subsubsection}{\numberline {1.3.1}Spannungswahl treffen}{4}{}%
\contentsline {subsubsection}{\numberline {1.3.2}Energie Kalibrierung}{5}{}%
\contentsline {subsubsection}{\numberline {1.3.3}Endpunktenergie determinieren}{6}{}%
\contentsline {section}{\numberline {2}Aufnahme von kosmischen Myonen}{8}{}%
\contentsline {subsubsection}{\numberline {2.0.1}Vergleich der Spektren}{8}{}%
\contentsline {subsection}{\numberline {2.1}Coincidence Aufnahmen}{8}{}%
\contentsline {subsubsection}{\numberline {2.1.1}Justieren des Schwellenwertes}{9}{}%
\contentsline {subsubsection}{\numberline {2.1.2}Justieren der Spannung}{9}{}%
\contentsline {subsubsection}{\numberline {2.1.3}Unbeabsichtigte Inzidenzen}{10}{}%
\contentsline {subsubsection}{\numberline {2.1.4}Zeitkalibrierung}{10}{}%
\contentsline {subsubsection}{\numberline {2.1.5}Ausrichting der Szintillatoren}{11}{}%
\contentsline {subsubsection}{\numberline {2.1.6}Kosmische Myonen}{13}{}%
\contentsline {section}{\numberline {3}Diskussion}{14}{}%
\contentsline {subsection}{\numberline {3.1}Anorganischer Szintillator}{14}{}%
\contentsline {subsection}{\numberline {3.2}Organischer Szintillator}{14}{}%
