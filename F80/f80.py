import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
import scipy.stats as stats
import matplotlib.pyplot as plt
import uncertainties.unumpy as unp
import uncertainties as unc
import scipy.constants as const
import math

## Settings ##
plt.style.use('classic')
interval = 1025
r = lambda x: (x.replace(',', '.'))

def gaussian_fit(x, a, sigma, mu):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))

def linear_fit(x, a, b):
    return a*x + b

def exp_fit(x, K, N):
    return pow(x*K, N)

def int_fit(x, a, b):
    return a*np.exp(-b*x)

def convert_data(filename, nrow):
    dt = pd.read_csv(filename, sep='\s+', header=None, decimal=',')
    return dt.to_numpy()[nrow][:interval]

def calc_error(a, b):
    return 1/2 * (a-b)

def conv_channel_to_energy(x):
    return a*x+b

def chisquareValue(xvalue, yvalues, sigma_y, fit_parameter, function):
    "return chi2/ndf value of fit for parameter function 'function'"
    chi2=0
    for i in range(len(xvalue)):
        expected = function(xvalue[i],*fit_parameter)
        chi2+=((expected-yvalues[i])/sigma_y[i])**2
    prob = round(1 - stats.chi2.cdf(chi2, len(xvalue) - len(fit_parameter)), 4) * 100
    return chi2/(len(xvalue)-len(fit_parameter)), prob #chis2 per degree of freedom and probability

def Fermifunction (x):
    "Fermifunktion/Coulombkorrektur"
    if(x==0):
        return 0
    F=2*np.pi*(Z*(x+m_e)/(1/alpha*math.sqrt(x*x+2*m_e*x)))/(1-math.exp(-2*np.pi*(38*(x+m_e)/(1/alpha*math.sqrt(x*x+2*m_e*x)))))
    return F

def f80_readfile(filename):
    "read file contents and return numpy array with file contents or, in case of corrected beta spectrum, with Kurie-transformed file contents."
    data=[]
    y_uncertainty=[]
    for line in open(filename): #open file and read file contents
        if(len(line.split())>100):
            temp = line.replace(",", ".")
            columns = [float(x) for x in temp.split()]
            #columns = temp.split(    ) # "    " is the TAB symbol
            dataarray=np.array(columns)
            break
        else:
            columns = line.split(    ) # "    " is the TAB symbol
            energy=float(columns[0])
            counts=float(columns[2])
            if(counts==0):
                data.append(0)
                y_uncertainty.append(0)
            else:
                # conversion to fermi variable
                KuriePlotData=math.sqrt(counts/(Fermifunction(energy)*(energy+m_e)*math.sqrt(energy*energy+2*energy*m_e)))
                sigmaKuriePlotData = math.sqrt(1./(counts*Fermifunction(energy)*(energy+m_e)*math.sqrt(energy*energy+2*energy*m_e))) * math.sqrt(counts) # gaussian error propagation, ussing possonian error on counts
                data.append(KuriePlotData)
                y_uncertainty.append(sigmaKuriePlotData)
            dataarray=np.array(data)
    return dataarray #convert data list to numpy array

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))


m_e=511.
alpha=1./137
Z=39
pi=const.pi


## 4.2 ##
bin = np.linspace(0, 1024, 1025)
Cs_450V = convert_data('4.3/Cs_450V_raw.dat', 0)
popt_450, pcov_450 = curve_fit(gaussian_fit, bin[730:850], Cs_450V[730:850], sigma=np.sqrt(Cs_450V[730:850]), maxfev=2000, p0=[320, 10, 770])

plt.clf()
plt.plot(bin, Cs_450V, marker='.', color='darkred', markersize=5, linewidth=0, label='Cs Messwerte')
plt.plot(bin, gaussian_fit(bin, *popt_450), marker=None, ls='--', alpha=0.5, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_450[2], 1), round(popt_450[1], 1)))
plt.vlines(popt_450[2], 0, np.amax(gaussian_fit(bin, *popt_450)), colors='darkblue', label='Photopeak')
plt.axvspan(100, 700, alpha=0.5, color='gray', label='Comptonspektrum')


plt.title(r'Energiespektrum von $ ^{137}Cs$ bei 450V', size=20)
plt.xlabel('Kanalnummer', size=15)
plt.ylabel('Zählrate', size=15)
plt.legend(frameon=True, loc='best')
plt.grid(True)
plt.tight_layout()
plt.savefig('results/4_2_Cs_450V.pdf', format='PDF')
#plt.show()

## 4.3 ##

Cs_435V = convert_data('4.3/Cs_435V_raw.dat', 0)
Cs_420V = convert_data('4.3/Cs_420V_raw.dat', 0)
Cs_405V = convert_data('4.3/Cs_405V_raw.dat', 0)
Cs_380V = convert_data('4.3/Cs_380V_raw.dat', 0)
Cs_370V = convert_data('4.3/Cs_370V_raw.dat', 0)

popt_370, pcov_370 = curve_fit(gaussian_fit, bin[160:230], Cs_370V[160:230], sigma=np.sqrt(Cs_370V[160:230]), maxfev=2000, p0=[550, 20, 200])
popt_380, pcov_380 = curve_fit(gaussian_fit, bin[220:260], Cs_380V[220:260], sigma=np.sqrt(Cs_380V[220:260]), maxfev=2000, p0=[410, 20, 250])
popt_405, pcov_405 = curve_fit(gaussian_fit, bin[390:460], Cs_405V[390:460], sigma=np.sqrt(Cs_405V[390:460]), maxfev=2000, p0=[250, 20, 420])
popt_420, pcov_420 = curve_fit(gaussian_fit, bin[510:620], Cs_420V[510:620], sigma=np.sqrt(Cs_420V[510:620]), maxfev=2000, p0=[170, 20, 560])
popt_435, pcov_435 = curve_fit(gaussian_fit, bin[680:730], Cs_435V[680:730], sigma=np.sqrt(Cs_435V[680:730]), maxfev=2000, p0=[350, 20, 705])



plt.clf()
fig, ax = plt.subplots(nrows=3, ncols=2, sharex=True)
fig.subplots_adjust(hspace=0)
ax[0][0].plot(bin, Cs_370V, marker='.', color='darkred', markersize=3, linewidth=0, label='370V')
ax[0][1].plot(bin, Cs_380V, marker='.', color='darkred', markersize=3, linewidth=0, label='380V')
ax[1][0].plot(bin, Cs_405V, marker='.', color='darkred', markersize=3, linewidth=0, label='405V')
ax[1][1].plot(bin, Cs_420V, marker='.', color='darkred', markersize=3, linewidth=0, label='420V')
ax[2][0].plot(bin, Cs_435V, marker='.', color='darkred', markersize=3, linewidth=0, label='435V')
ax[2][1].plot(bin, Cs_450V, marker='.', color='darkred', markersize=3, linewidth=0, label='450V')

ax[0][0].plot(bin, gaussian_fit(bin, *popt_370), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_370[2], 1), round(popt_370[1], 1)))
ax[0][1].plot(bin, gaussian_fit(bin, *popt_380), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_380[2], 1), round(popt_380[1], 1)))
ax[1][0].plot(bin, gaussian_fit(bin, *popt_405), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_405[2], 1), round(popt_405[1], 1)))
ax[1][1].plot(bin, gaussian_fit(bin, *popt_420), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_420[2], 1), round(popt_420[1], 1)))
ax[2][0].plot(bin, gaussian_fit(bin, *popt_435), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_435[2], 1), round(popt_435[1], 1)))
ax[2][1].plot(bin, gaussian_fit(bin, *popt_450), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_450[2], 1), round(popt_450[1], 1)))

for i in range(0, 3):
    for j in range(0,2):
        if i == 2 : ax[i][j].set_xlabel('Kanal', size=15)
        #ax[i][j].set_ylabel('Zählrate', size=15)
        ax[i][j].grid(True)
        ax[i][j].legend(frameon=True, fontsize=7)

fig.suptitle(r'Zählrate als Funktion des Kanals mit Gaußfit', size=25)

fig.tight_layout()
#plt.savefig('results/4_3_Abb.pdf', format='PDF')
plt.clf()

## 4.3: 4) Plot the pulse height as function of the PM voltage ##
pulse_height = unp.uarray([popt_370[2], popt_380[2], popt_405[2], popt_420[2], popt_435[2], popt_450[2]], [popt_370[1], popt_380[1], popt_405[1], popt_420[1], popt_435[1], popt_450[1]])
voltage = np.array([370, 380, 405, 420, 435, 450])
popt, pcov = curve_fit(linear_fit, voltage, unp.nominal_values(pulse_height), sigma=unp.std_devs(pulse_height))
popt2, pcov2 = curve_fit(exp_fit, voltage, unp.nominal_values(pulse_height), sigma=unp.std_devs(pulse_height))

chi2_ndf, prob = chisquareValue(voltage, unp.nominal_values(pulse_height), unp.std_devs(pulse_height), popt, linear_fit)

chi2_ndf_2, prob2 = chisquareValue(voltage, unp.nominal_values(pulse_height), unp.std_devs(pulse_height), popt2, exp_fit)


plt.errorbar(voltage, unp.nominal_values(pulse_height), xerr=voltage*1e-4, yerr=unp.std_devs(pulse_height), linewidth=0, marker='.', color='darkred', label='Messwerte')
plt.plot(voltage, linear_fit(voltage, *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade a={0} b={1}'.format(round(popt[0], 1), round(np.sqrt(pcov[0][0]), 1)))
plt.plot(voltage, exp_fit(voltage, *popt2), marker=None, ls='--', color='darkgreen', label='Exponentialfit K={0} N={1}'.format(round(popt2[0], 4), round(popt2[1], 1)))
plt.text(400, 200, r'linear: $\chi_[n.d.f]$ = {0}'.format(round(chi2_ndf, 1)), bbox=dict(facecolor='gray', alpha=0.5), fontsize=15)
plt.text(400, 300, r'exp: $\chi_[n.d.f]$ = {0}'.format(round(chi2_ndf_2, 1)), bbox=dict(facecolor='gray', alpha=0.5), fontsize=15)
plt.title(r'Zählrate als Funktion der Spannung', size=15)
plt.ylabel('Zählrate', size=15)
plt.xlabel('Spannung [V]', size=15)
plt.grid(True)
plt.legend(frameon=True, loc='best')
plt.tight_layout()
#plt.savefig('results/4_3_Abb2.pdf', format='PDF')
#plt.show()

## 4.3.1 ##
voltage = np.array([450, 440, 430, 420, 410])
E = unp.uarray([495, 409, 338, 564, 462], calc_error(np.array([529, 442, 372, 614, 508]), np.array([456, 369, 303, 502, 422])))
Delta_E = unp.std_devs(E)/unp.nominal_values(E)
print(Delta_E)
print('Minimaler DE/E bei U = {0}'.format(voltage[np.where(np.amin(unp.nominal_values(Delta_E)))]))

## 4.3.2 ##
Cs_410V = convert_data('4.3.2/432Cs_410V_raw.dat', 0)
Co_410V = convert_data('4.3.2/432Co_410V_raw.dat', 0)

popt_gamma_1, pcov_gamma_1 = curve_fit(gaussian_fit, bin[380:420], Co_410V[380:420], sigma=np.sqrt(Co_410V[380:420]), maxfev=2000, p0=[100, 10, 400])
popt_gamma_2, pcov_gamma_2 = curve_fit(gaussian_fit, bin[430:480], Co_410V[430:480], sigma=np.sqrt(Co_410V[430:480]), maxfev=2000, p0=[80, 10, 455])

popt_Cs, pcov_Cs = curve_fit(gaussian_fit, bin[200:260], Cs_410V[200:260], sigma=np.sqrt(Cs_410V[200:260]), p0=[630, 10, 235])

plt.clf()
fig, ax = plt.subplots(nrows=2, ncols=1)

fig.suptitle(r'Enerige Kalibrierung $^{60}Co$ und $^{137}Cs$', size=15)

ax[0].plot(bin, Co_410V, marker='.', color='darkred', markersize=3, linewidth=0, label=r'$^{60}Co$')
ax[1].plot(bin, Cs_410V, marker='.', color='darkred', markersize=3, linewidth=0, label=r'${137}{Cs}$')

ax[0].plot(bin, gaussian_fit(bin, *popt_gamma_1), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_gamma_1[2], 1), round(popt_gamma_1[1], 1)))
ax[0].plot(bin, gaussian_fit(bin, *popt_gamma_2), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_gamma_2[2], 1), round(popt_gamma_2[1], 1)))

ax[1].plot(bin, gaussian_fit(bin, *popt_Cs), marker=None, ls='--', alpha=0.8, color='darkblue', linewidth=2, label='$\mu$ = {0},\n $\sigma$ = {1}'.format(round(popt_Cs[2], 1), round(popt_Cs[1], 1)))

ax[0].vlines(popt_gamma_1[2], 0, popt_gamma_1[0], colors='red', label=r'$\gamma_1 Peak$')
ax[0].vlines(popt_gamma_2[2], 0, popt_gamma_2[0], colors='yellow', label=r'$\gamma_2 Peak$')

ax[1].vlines(popt_Cs[2], 0, popt_Cs[0], colors='red', label=r'$\gamma Peak$')

for a in ax:
    a.grid(True)
    a.legend(frameon=True, fontsize=10)
    a.set_xlabel('Kanal', size=15)
    a.set_ylabel('Zählrate', size=15)

fig.tight_layout()
#plt.savefig('results/4_3_1_Abb.pdf', format='PDF')
#plt.show()

plt.clf()
plt.grid(True)
energy = np.array([1.1733, 1.3325, 0.66166]) #MeV
channel = np.array([popt_gamma_1[2], popt_gamma_2[2], popt_Cs[2]])
popt, pcov = curve_fit(linear_fit, channel, energy)
chi2_ndf, pvalue = chisquareValue(channel, energy, [pcov_gamma_1[1,1], pcov_gamma_2[1,1], pcov_Cs[1,1]], popt, linear_fit)

plt.errorbar(channel, energy, xerr=1e-4, linewidth=0, marker='.', markersize=10, color='darkred', label='Photopeaks')
plt.plot(channel, linear_fit(channel, *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade, a={0}, b={1}'.format(round(popt[0], 4), round(popt[1], 3)))
plt.text(350, 0.8, r'$\chi_[n.d.f]$ = {0}, P = {1}%'.format(round(chi2_ndf, 1), pvalue), bbox=dict(facecolor='gray', alpha=0.5), fontsize=15)
plt.title('Energiekalibration', size=20)
plt.xlabel('Kanal', size=15)
plt.ylabel('Energie', size=15)
plt.tight_layout()
plt.legend(frameon=True, loc='best')
#plt.show()
plt.savefig('results/4_3_2_Abb.pdf', format='PDF')

a = unc.ufloat(popt[0], np.sqrt(pcov[0, 0]))
b = unc.ufloat(popt[1], np.sqrt(pcov[1, 1]))
print('\n 4.3.2 Ausgleichsgerade')
print('a = ' + str(a))
print('b = ' + str(b))

## 4.3.3##
Sr = convert_data('4.3.3/433Sr_410V_raw.dat', 0)
bg = convert_data('4.3.3/433BG_410V_raw.dat', 0)
Sr_corr = Sr-bg

# plt.clf()
# plt.plot(bin, Sr_corr, marker='.', color='darkred', linewidth=0, label=r'Korrigiertes $^{90}Sr$ Spektrum')
# plt.xlabel('Kanal', size=15)
# plt.ylabel('Zählrate', size=15)
# plt.xlim(0, 1024)
# plt.ylim(0, np.amax(Sr_corr))
#
# plt.title(r'Hintergrundkorrigiertes $^{90}Sr$ Spektrum', size=15)
# plt.grid(ls='dotted')
# plt.legend(frameon=True, loc='best')
#plt.show()
#plt.savefig('results/4_3_3_Abb.pdf', format='PDF')
plt.clf()

#### Aus der Vorlage ####

xdata = np.array([1.1733, 1.3325, 0.66166]) #MeV
ydata = np.array([popt_gamma_1[2], popt_gamma_2[2], popt_Cs[2]])
y_uncertainty = np.array([popt_gamma_1[1], popt_gamma_2[1], popt_Cs[1]])

def func(x, a):
    return a*x

fit_parameter, covariance_matrix = curve_fit(func, xdata, ydata,absolute_sigma=True, sigma=y_uncertainty)
steigung = "$m=$ ("+str(np.round(fit_parameter[0],1))+'$\\pm$ '+str(np.round(np.sqrt(covariance_matrix[0][0]),1))+') 1/MeV'
pointplot = plt.errorbar(xdata, ydata, y_uncertainty,
                         fmt='.', linewidth=1,
                         linestyle='', color='black',
                         label='Messpunkte mit Fehler')
x = np.linspace(0, max(xdata))
fitplot  = plt.plot(x, func(x, fit_parameter[0]), 'r-',
                    color='red', label='Linearer Fit')
plt.text(0.7,150,'%s'%(steigung),
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':10},
        fontsize=13)
plt.xlabel('Energie [MeV]', fontsize=13)
plt.ylabel('Pulsposition [Channel]', fontsize=13)
plt.title('Energiekalibrierung', fontsize=16)
plt.legend(frameon=True, fontsize = 12)
plt.grid(True)
#plt.savefig('results/4_3_3_Abb1.pdf' ,format='PDF')
#plt.show()
plt.close()

## Kurie-Plots##
background = f80_readfile('4.3.3/433BG_410V_raw.dat')


##############################################################################
                                 #Sr90_Al10#
##############################################################################
ydataarray=f80_readfile('4.3.3/433Sr_410V_raw.dat') - background
a=unp.nominal_values(a) #calibration slope. Default value (no calibration): a=1
b=0
xdataarray = np.arange(0,len(ydataarray))*a+b

def linfunc(x, a, b):
    return a*x + b

fitxmin=300
fitxmax=590

m,b = np.polyfit(xdataarray[fitxmin:fitxmax], ydataarray[fitxmin:fitxmax],1)

print(m,b)

x = np.linspace(0, max(xdataarray), 1000)
fitplot  = plt.plot(x, linfunc(x, m, b), ls='--', marker=None, color='darkblue', label='Linearer Fit')
plt.plot(xdataarray,ydataarray,linewidth=0, marker='.', color='darkred', label='Messkurve')
plt.grid(True)
plt.ylabel('Kurie Variable', fontsize=13)
plt.xlabel('Energie [MeV]', fontsize=13)
xmin=0
xmax=2
plt.xlim((xmin,xmax)) #restrict x axis to [xmin,xmax]
axes = plt.gca()
axes.set_ylim([0,1.1*max(ydataarray)])
endpointEnergy=-b/m
plt.text(xmax*0.4,max(ydataarray),
         'Endpoint Energy = '+str(round(endpointEnergy,2))+' MeV',
         fontsize=16)
plt.title('Kurieplot von $^{90}Sr$-Spektrum',
          fontsize=16)
E1=endpointEnergy
plt.legend(frameon=True, loc='right', fontsize = 12)
plt.savefig('results/4_3_3_1mm.pdf',format='pdf')
#plt.show()
plt.close()

##############################################################################
                                 #Sr90_Al15#
##############################################################################

ydataarray=f80_readfile('4.3.3/433Al_15_raw.dat') - background
b=0
xdataarray = np.arange(0,len(ydataarray))*a+b

#fitrange
fitxmin=250
fitxmax=450

m,b = np.polyfit(xdataarray[fitxmin:fitxmax], ydataarray[fitxmin:fitxmax],1)

print(m,b)

x = np.linspace(0, max(xdataarray), 1000)
fitplot  = plt.plot(x, linfunc(x, m, b), ls='--', marker=None, color='darkblue', label='Linearer Fit')
plt.plot(xdataarray,ydataarray,linewidth=0, marker='.', color='darkred', label='Messkurve')
plt.grid(True)
plt.ylabel('Kurie Variable', fontsize=13)
plt.xlabel('Energy', fontsize=13)
plt.xlim((xmin,xmax)) #restrict x axis to [xmin,xmax]
axes = plt.gca()
axes.set_ylim([0,1.1*max(ydataarray)])
endpointEnergy=-b/m
plt.text(xmax*0.4,max(ydataarray),
         'Endpoint Energy = '+str(round(endpointEnergy,2))+' MeV',
         fontsize=16)
plt.title('Kurieplot von $^{90}Sr$ Spektrum mit 0.5mm Al',
          fontsize=16)
E2=endpointEnergy
plt.legend(frameon=True, loc='right', fontsize = 12)
plt.savefig('results/4_3_3_15mm.pdf', format='pdf')
#plt.show()
plt.close()

##############################################################################
                                 #Sr90_Al20#
##############################################################################

ydataarray=f80_readfile('4.3.3/433Al_20_raw.dat') - background
b=0
xdataarray = np.arange(0,len(ydataarray))*a+b

#fitrange
fitxmin=200
fitxmax=390

m,b = np.polyfit(xdataarray[fitxmin:fitxmax], ydataarray[fitxmin:fitxmax],1)

print(m,b)

x = np.linspace(0, max(xdataarray), 1000)
fitplot  = plt.plot(x, linfunc(x, m, b), ls='--', marker=None, color='darkblue', label='Linearer Fit')
plt.plot(xdataarray,ydataarray,linewidth=0, marker='.', color='darkred', label='Messkurve')
plt.grid(True)
plt.ylabel('Kurie Variable', fontsize=13)
plt.xlabel('Energy', fontsize=13)

plt.xlim((xmin,xmax)) #restrict x axis to [xmin,xmax]
axes = plt.gca()
axes.set_ylim([0,1.1*max(ydataarray)])
endpointEnergy=-b/m
plt.text(xmax*0.4,max(ydataarray),
         'Endpoint Energy = '+str(round(endpointEnergy,2))+' MeV',
         fontsize=16)
plt.title('Kurieplot von $^{90}Sr$ Spektrum mit 1mm Al',
          fontsize=16)
E3=endpointEnergy
plt.legend(frameon=True, loc='right', fontsize = 12)
plt.savefig('results/4_3_3_20mm.pdf',format='pdf')
#plt.show()
plt.close()

##############################################################################
                                 #Sr90_Al25#
##############################################################################
ydataarray=f80_readfile('4.3.3/433Al_25_raw.dat') - background
b=0
xdataarray = np.arange(0,len(ydataarray))*a+b

#fitrange
fitxmin=105
fitxmax=350

m,b = np.polyfit(xdataarray[fitxmin:fitxmax], ydataarray[fitxmin:fitxmax],1)

print(m,b)

x = np.linspace(0, max(xdataarray), 1000)
fitplot  = plt.plot(x, linfunc(x, m, b), ls='--', marker=None, color='darkblue', label='Linearer Fit')
plt.plot(xdataarray,ydataarray,linewidth=0, marker='.', color='darkred', label='Messkurve')
plt.grid(True)
plt.ylabel('Kurie Variable', fontsize=13)
plt.xlabel('Energy', fontsize=13)

plt.xlim((xmin,xmax)) #restrict x axis to [xmin,xmax]
axes = plt.gca()
axes.set_ylim([0,1.1*max(ydataarray)])
endpointEnergy=-b/m
plt.text(xmax*0.4,max(ydataarray), 'Endpoint Energy = '+str(round(endpointEnergy,2))+' MeV', fontsize=16)
plt.title('Abb. [21]: Kurieplot von $^{90}Sr$ Spektrum mit 1.5mm Al', fontsize=16)
E4=endpointEnergy
plt.legend(frameon=True, loc='right', fontsize = 12)
plt.savefig('results/4_3_3_25mm.pdf',format='pdf')
#plt.show()
plt.close()

##############################################################################
                                 #Sr90_Al30#
##############################################################################
ydataarray=f80_readfile('4.3.3/433Al_30_raw.dat') - background
b=0 #b wird immer wieder neu definiert
xdataarray = np.arange(0,len(ydataarray))*a+b

#fitrange
fitxmin=105
fitxmax=300

m,b = np.polyfit(xdataarray[fitxmin:fitxmax], ydataarray[fitxmin:fitxmax],1)

print(m,b)

x = np.linspace(0, max(xdataarray), 1000)
fitplot  = plt.plot(x, linfunc(x, m, b), ls='--', marker=None, color='darkblue', label='Linearer Fit')
plt.plot(xdataarray,ydataarray,linewidth=0, marker='.', color='darkred', label='Messkurve')
plt.grid(True)
plt.ylabel('Kurie Variable', fontsize=13)
plt.xlabel('Energy', fontsize=13)

plt.xlim((xmin,xmax)) #restrict x axis to [xmin,xmax]
axes = plt.gca()
axes.set_ylim([0,1.1*max(ydataarray)])
endpointEnergy=-b/m
plt.text(xmax*0.4,max(ydataarray),
         'Endpoint Energy = '+str(round(endpointEnergy,2))+' MeV',
         fontsize=16)
plt.title('Kurieplot von $^{90}Sr$ Spektrum mit 2mm Al',
          fontsize=16)
plt.legend(frameon=True, loc='right', fontsize = 12)
E5=endpointEnergy
plt.savefig('results/4_3_3_30mm.pdf',format='PDF')
#plt.show()
plt.close()

##############################################################################
                                 #Maximalenergie#
##############################################################################
d = np.array([1, 1.5, 2, 2.5, 3])
E = np.array([E1, E2, E3, E4, E5])
E_err = 0.05*E
popt, pcov = curve_fit(int_fit, d, E, sigma=E_err)
chi2_ndf, prob = chisquareValue(unp.nominal_values(d), E, E_err, popt, int_fit)
plt.clf()
plt.errorbar(d, E, yerr=E_err, marker='.', linewidth=0, color='darkred', label='Maximalenergiewerte')
plt.plot(d, int_fit(d, *popt), marker=None, ls='--', color='darkblue', label='Exponentialer Fit a={0}'.format(round(popt[0],2)))
plt.text(1.25, 0.8, r'$\chi_[n.d.f]$ = {0}, P = {1} %'.format(round(chi2_ndf, 1), pvalue), bbox=dict(facecolor='gray', alpha=0.5), fontsize=15)
plt.title('Maximalenergieextrapolation', size=15)
plt.xlabel('Aluminiumdicke [mm]', size=15)
plt.ylabel('Energie [MeV]', size=15)
plt.tight_layout()
plt.legend(frameon=True, loc='best')
plt.grid(True)
plt.savefig('results/Maximalenergie.pdf',format='PDF')
#plt.show()

a = unc.ufloat(popt[0], np.sqrt(pcov[0,0]))
b = unc.ufloat(popt[1], np.sqrt(pcov[1,1]))
print(a)
print(b)

##############################################################################
                                 #Myonen#
##############################################################################

#######
#5.1.4#
#######
Cs = convert_data('5.1.4/514_1400BG_raw.dat', 0)
plt.clf()
plt.plot(bin, Cs)
#plt.show()

#######
#5.1.5#
#######

delay = np.array([8, 16, 32, 36, 40, 48, 56]) #ns
peak = unp.uarray([94, 160, 293, 327, 361, 427, 495], [5, 5, 5, 5, 5, 5, 5])
popt, pcov = curve_fit(linear_fit, unp.nominal_values(peak), delay)

a = unc.ufloat(popt[0], np.sqrt(pcov[0,0]))
b = unc.ufloat(popt[1], np.sqrt(pcov[1,1]))

plt.clf()
plt.grid(True)
plt.errorbar(unp.nominal_values(peak), delay, xerr=unp.std_devs(peak), marker='.', linewidth=0, color='darkred', label='Messwerte')
plt.plot(unp.nominal_values(peak), linear_fit(unp.nominal_values(peak), *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')

plt.title('Zeitkalibrierung', size=15)
plt.ylabel('Verzögerung [ns]', size=15)
plt.xlabel('Peakposition [Kanal]', size=15)
plt.legend(frameon=True, loc='best')
plt.text(150, 8, 'f(x) = ({0}) * x  ({1})'.format(a, b), bbox=dict(facecolor='gray', alpha=0.5), fontsize=15)
#plt.savefig('results/Zeitkalibrierung.pdf', format='PDF')
#plt.show()


print('\nDie Steigung für 5.1.5 beträgt ' + str(a))
print('Die Verschiebung ist' + str(b))
N_1 = unc.ufloat(1700, np.sqrt(1700))
N_2 = unc.ufloat(109468, np.sqrt(109468))
sigma = a*(290 - 265)*1e-9*3*60
R_acc = sigma * N_1 * N_2
R_counts = 10
print(R_acc)
print(calc_sigma(R_acc, R_counts))

#######
#5.1.6#
#######
time = a*bin+b
parallel = convert_data('5.1.6/516parallel_raw.dat', 0)
anti_parallel = convert_data('5.1.6/516antiparallel_raw.dat', 0)

parallel_mean = sum(parallel[200:320]/sum(parallel[200:320])*unp.nominal_values(time[200:320]))
anti_parallel_mean = sum(anti_parallel[200:320]/sum(anti_parallel[200:320])*unp.nominal_values(time[200:320]))

plt.clf()
fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True)
fig.subplots_adjust(hspace=0)
ax[0].bar(unp.nominal_values(time), parallel, width=0.1, color='darkred', edgecolor='darkred')
ax[0].vlines(parallel_mean, 0, 6, label=r'Mittelwert $\mu$ = '+str(np.round(parallel_mean, 2)))
ax[1].bar(unp.nominal_values(time), anti_parallel, width=0.1, color='darkred', edgecolor='darkred')
ax[1].vlines(anti_parallel_mean, 0, 8, label=r'Mittelwert $\mu$ = '+str(np.round(anti_parallel_mean, 2)))
ax[1].set_xlabel('Verzögerung [ns]', size=15)

for a in ax:
    a.grid(True)
    a.legend(frameon=True, loc=2)
    a.set_ylabel('Zählrate', size=15)

fig.suptitle('Zählrate mit paralleler / antiparalleler Ausrichtung', size=15)
plt.xlim(unp.nominal_values(time[200]), unp.nominal_values(time[300]))
plt.tight_layout()
#plt.savefig('results/Parallel_Antiparallel.pdf', format='PDF')
#plt.show()

print(sum(parallel[220:320]))
print(sum(anti_parallel[220:320]))

r_1 = unc.ufloat(sum(parallel[220:320]), 0) / (10)         #/m
r_2 = unc.ufloat(sum(anti_parallel[220:320]), 0) / (10)    #/m

b = unc.ufloat(5, 0.25)    #cm
l = unc.ufloat(10, 0.5)     #cm

n_1 = r_1 / b / l
n_2 = r_2 / b / l

n_lit = 1   #/min/cm^2

print(str(n_1) + ' Abweichung: ' + str(calc_sigma(n_1, n_lit)))
print(str(n_2) + ' Abweichung: ' + str(calc_sigma(n_2, n_lit)))
